import os
import argparse
from time import time

def download_video(url, filename, ext):
    file = filename + "." + ext

    # download from Youtube URL
    if (url):
        print("Downloading Youtube Video")
        os.system("youtube-dl -o " + file + " -f " + ext + " " + url)
        print("Done..")

    else:
        print("Error")

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-u", "--url", required=True, help="youtube url")
    ap.add_argument("-e", "--ext", required=True, help="video extension")
    args = vars(ap.parse_args())

    # https://www.youtube.com/watch?v=usE5_Wii6x0
    url = args["url"]
    ext = args["ext"]
    filename = str(time())

    download_video(url, filename, ext)