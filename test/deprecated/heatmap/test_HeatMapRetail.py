import unittest
import sys
import os
cpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(cpath, '..', '..'))
from witcloud.modules.RetailMetrics import RetailMetrics
from witcloud.models.Camera import Camera

class HeatMapRetailTest(unittest.TestCase):
    def test_heatmap_retail(self):
        camera = Camera({
            "description": "Desc",
            "enable": True,
            "debug": True,

            "center_id": 1,
            "center": "Main center",
            "node_id": 1,
            "cam_id": 1029,
            "system_id": 12,

            "url": "./mall.mp4",
            "sw_grab_image": True,
            "duration": 10,
            "fps_grab_img": 5.0,
            "MAX_RESOLUTION": "640x360",

            "detection_max_distance": 10000.0,
            "detection_ppf": 10.0,
            "labels": ["person"],
            "detection_confidence": 0.3,
            "gpu_url": "http://185.142.223.38:8080/image",
            "mosaic_size": 9,

            "timezone": "SouthAmerica/Asunción",
            "schedule": {},

            "api_server": "http://192.168.5.105:8080/_ah/api",
            "api_version": "v2",
            "api_name": "witcloud",

            "enable_save_video": False,
            "frame_history": 500,
            "frames_to_record": 200,
            "min_percent_area": 0.1,
            "motion_threshold": 80,

            "enable_roi": False,
            "roi_list": [],

            "notification_server": "http://130.211.62.114:8081",
            "alerts": []
        })
        instance = RetailMetrics(camera)
        instance.start()
        instance.join()

if __name__ == "__main__":
    unittest.main()
