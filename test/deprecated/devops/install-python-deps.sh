#!/bin/bash
ROOT_FOLDER=`pwd`
MODULE="witcloud"
#----------------------------------
echo "ROOT_FOLDER: " $ROOT_FOLDER
echo "MODULE: " $MODULE

echo "Creating virtual environment"
sudo apt-get update
pip3 install virtualenv
pip3 install virtualenvwrapper

export WORKON_HOME=$HOME/.virtualenvs
mkdir -p $WORKON_HOME
source /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv --python=`which python3` $MODULE

echo "Installing libraries"
pip3 install -r requirements.txt
echo "Done"
