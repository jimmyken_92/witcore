# Witcloud

## test/emotion

- `devops/`: run install-system-deps.sh and install-python-deps.sh
- `data.json`: Add the name, description, youtube URL, extension and expected output in the .json format. You can add more than one source there.
- `test_EmotionDetector.py`: run with the following `python test_EmotionDetector.py`. The command starts TestEmotion(unittest.TestCase) and:
  - Create a folder in `/tmp/videosource/`
  - Create `result/*name_of_video*.json` to test the output.
  - Use the `youtube-dl` library to download the video(or videos) in `data.json`
  - Start the following components: VideoReader, FaceRecognition, EmotionDetector, DetectorDrawer, DetectionPacker and VideoWriter.
- `VideoReader`: get every frame of the video source.
- `FaceRecognition`: detect faces in a frame.
- `EmotionDetector`: detect the emotion of the faces detected. Place the result data in a `Detection` model notify.
- `DetectorDrawer`: draw a rectangle and put the text that comes in `lst_detections` and `detection.roi_id` via notify in the source frames.
- `DetectionPacker`: write the data in a .json file.
- `VideoWriter`: create a video with the source frames.

## How to Run with Docker

Build: Run these commands at the root of the `Witcloud` project

    docker build -t witcloud-emotion -f test/emotion/Dockerfile .

Run:
    docker run -d witcloud-emotion
