# Witcloud

## test/face_recognition:

- `devops/`: run install-system-deps.sh and install-python-deps.sh
- `data.json`: Add the name, description, youtube URL, extension and expected output in the .json format. You can add more than one source there.
- `test_faceRecognition.py`: run with the following `python test_faceRecognition.py`. The command starts TestFaceRecognition(unittest.TestCase) and:
	- Create a folder in `/tmp/videosource/`
	- Create `result/*name_of_video*.json` to test the output.
	- Use the `youtube-dl` library to download the video(or videos) in `data.json`
	- Start the following components: VideoReader, FaceRecognition, FaceComparator, DetectorDrawer, DetectionPacker and VideoWriter.
- `VideoReader`: get every frame of the video source.
- `FaceRecognition`: detect faces in a frame.
- `FaceComparator`: compare the faces detected in previous frames with the faces detected in this frame. If the `FaceComparator` detect the same face or faces, add an id to that face. The id is the hexadecimal encoded array of the face. Place the result data in a `Detection` model notify.
- `DetectorDrawer`: draw a rectangle and put the text that comes in `lst_detections` and `detection.roi_id` via notify in the source frames.
- `DetectionPacker`: write the data in a .json file.
- `VideoWriter`: create a video with the source frames.

