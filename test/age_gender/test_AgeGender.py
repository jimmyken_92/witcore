import unittest
import sys
import os
cpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(cpath, '..', '..'))
import json

from witcloud.modules.AgeGenderDetector import AgeGenderDetector
from witcloud.modules.FaceDetector import FaceDetector
# from witcloud.modules.VideoReader import VideoReader
from witcloud.modules.OpenCV_VideoReader import OpenCV_VideoReader as VideoReader
from witcloud.modules.DetectorDrawer import DetectorDrawer
from witcloud.modules.DetectionPacker import DetectionPacker
from witcloud.modules.VideoWriter import VideoWriter

class TestAgeGender(unittest.TestCase):
	def test_age_gender(self):
		# set the source path and the data path
		filepath = "/tmp/videosource/"
		jsonpath = os.path.join(cpath, "data.json")

		# videoReader test parameters
		cam_id = 1029
		fps_grab_img = 1.0

		# check if the filepath folder exists
		# if not, create the folder
		if not os.path.exists(filepath):
			os.makedirs(filepath)

		# start reading data.json
		with open(jsonpath) as json_file:  
			source = json.load(json_file)

			# start a loop reading each object in the file
			for src in source["source"]:
				# create/set the output folder
				folder_name = "result/"
				if not os.path.exists(folder_name):
					os.makedirs(folder_name)

				# create the video path and the output .json path
				file = filepath + src["name"] + "." + src["ext"]
				detection_file = folder_name + src["name"] + ".json"

				# if the (video) file doesn't exists, download it
				if not os.path.exists(file):
					print("INFO:Downloading...")
					# create the command using youtube-dl
					os.system("youtube-dl -o " + file + " -f " + src["ext"] + " " + src["url"])

				# set the expected output video
				videoWriterOutput = "/tmp/" + src["name"] + "." + src["ext"]

				# set components and classes
				components = []
				faceDetector = FaceDetector()
				ageGenderDetector = AgeGenderDetector()
				videoReader = VideoReader(filename=file, frame_rate=fps_grab_img)

				detectorDrawer = DetectorDrawer(output=folder_name)
				detectorPacker = DetectionPacker(filename=detection_file)
				videoWriter = VideoWriter(output=videoWriterOutput)

				# add components
				components.append(videoReader)
				components.append(detectorDrawer)

				# set subscriptions -> expected results
				videoReader.subscribe(faceDetector) # -> frame
				faceDetector.subscribe(ageGenderDetector) # -> (boxes, frame)
				ageGenderDetector.subscribe(detectorDrawer) # -> (lst_detections, frame)
				ageGenderDetector.subscribe(detectorPacker)
				detectorDrawer.subscribe(videoWriter)

				# start running components
				print("Started")
				for component in components:
					component.start()

				for component in components:
					component.join()
				print("Finished")

				# when finished, close the videoWriter
				videoWriter.close()
				# start writing detections in a .json file
				detectorPacker.dumpDetectionsToFile()

				# set the previous .json results
				test_json = src["testing_file"]
				# set the current .json results
				result_json = src["expected_output"]

				# check if the files exists
				assert os.path.exists(detection_file)
				assert os.path.exists(result_json)

				# get the percentage of equality between the two
				test_to_compare = []
				result_to_compare = []

				with open(test_json) as json_file:
					json_list = json.load(json_file)
					for obj in json_list:
						test_to_compare.append(obj.get("label"))

				with open(result_json) as json_file:
					json_list = json.load(json_file)
					for obj in json_list:
						result_to_compare.append(obj.get("label"))

				length = len(test_to_compare)+len(result_to_compare)
				average_length = length/2

				matches = len([x for x in test_to_compare if x in result_to_compare])
				percentage = int((matches/average_length)*100)

				print("-> Previous json length: {}".format(len(test_to_compare)))
				print("-> Results json length: {}".format(len(result_to_compare)))
				print("-> Equality: {}% ({} matches in {} average length)".format(percentage, matches, average_length))

if __name__ == "__main__":
	unittest.main()
