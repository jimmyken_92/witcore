# Witcloud

## test/tracking:

- `devops/`: run install-system-deps.sh and install-python-deps.sh
- `data.json`: Add the name, description, youtube URL, extension and expected output in the .json format. You can add more than one source there.
- `test_PeopleTracking.py`: run with the following `python test_PeopleTracking.py`. The command starts PeopleTracking(unittest.TestCase) and:
	- Create a folder in `/tmp/videosource/`
	- Create `result/*name_of_video*.json` to test the output.
	- Use the `youtube-dl` library to download the video(or videos) in `data.json`
	- Start the following components: VideoReader, FaceRecognition, BodyDetector, ObjectTracker, DetectorDrawer, DetectionPacker and VideoWriter.
- `VideoReader`: get every frame of the video source.
- `FaceRecognition`: detect faces in a frame.
- `BodyDetector`: detect bodies in a frame.
- `ObjectTracker`:
- `DetectorDrawer`: draw a rectangle and put the text that comes in `lst_detections` and `detection.roi_id` via notify in the source frames.
- `DetectionPacker`: write the data in a .json file.
- `VideoWriter`: create a video with the source frames.

