import matplotlib.patches as patches
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import numpy as np
import math
import skimage.draw
from skimage.transform import resize
# from skimage.draw import polygon_perimeter
import skimage.io as io
from io import BytesIO
from PIL import Image, ImageFont, ImageDraw
import base64

# https://github.com/scikit-image/scikit-image/issues/2688
def rectangle(r0, c0, width, height):
    rr, cc = [r0, r0 + width, r0 + width, r0], [c0, c0, c0 + height, c0 + height]
    return skimage.draw.polygon(rr, cc)

def rectangle_perimeter(c0, r0, width, height, shape=None, clip=False):
    rr, cc = [c0- height/2, c0- height/2, c0+ height/2, c0+ height/2], [r0- width/2, r0+ width/2, r0+ width/2, r0- width/2]
    return skimage.draw.polygon_perimeter(rr, cc, shape=shape, clip=clip)

def draw_text(image, n_element, label, color=(0, 255, 0), position=(0, 0)):
    # ---------------------- #
    
    # ADDING LABEL WITH TIME #
    # ---------------------- #
    # label_height = 40+20*n_element
    im = Image.fromarray(image)
    # pos_y_label = image.shape[0]- label_height
    draw = ImageDraw.Draw(im)
    # font = ImageFont.truetype(<font-file>, <font-size>)
    # draw.text((x, y),"Sample Text",(r,g,b))
        
    font=ImageFont.truetype("DejaVuSans.ttf", 20)
    draw.text(position, label, color ,font=font)
    # img.save('sample-out.jpg')
    image = np.asarray(im, dtype=np.uint8)
    image.setflags(write=1)
    return image

def draw_point(image, center_x, center_y, radius=5, color=(0, 255, 0)):
    if 0 <= center_x <= image.shape[0] and 0 <= center_y <= image.shape[1]:
        rr, cc = skimage.draw.circle(center_x, center_y, radius)
        image[rr, cc, :] = color
    return image

def draw_rectangle(image, pos_x, pos_y, width, height, color=(0, 255, 0)):
    """
        color = (r, g, b)
    """
    rr, cc = rectangle_perimeter( pos_y, pos_x, width, height, shape=image.shape)
    image[rr, cc, :] = color
    rr, cc = rectangle_perimeter( pos_y, pos_x, width-1, height-1, shape=image.shape)
    image[rr, cc, :] = color
    rr, cc = rectangle_perimeter( pos_y, pos_x, width-2, height-2, shape=image.shape)
    image[rr, cc, :] = color
    return image

def save_image(filename, image):
    """
        inputs:
            filename
            image
    """
    io.imsave(filename, image)

def convert_fig2array(fig):
    img = np.fromstring(fig.canvas.tostring_rgb(), dtype='uint8')
    im_size = fig.canvas.get_width_height()
    rgb = img.reshape(im_size[1], im_size[0], 3)
    return rgb

def read_image(filename, width=None, height=None):
    img = io.imread(filename)
    if width is not None and height is not None:
        #resize image
        img = resize(img, (height, width))
    return img

def compute_heatmap_matrix(list_detections, heatmap_width, heatmap_height, smooth_flag=False, scale=0.1):
    '''
        Compute heat map based on detection list.
    '''
    print("Computing heatmap with %s points" % len(list_detections))
    variance = 0.5
    radius = math.ceil(0.1*max(heatmap_width, heatmap_height))
    heatmap = [[0 for _ in range(heatmap_width+2*radius)]
                        for _ in range(heatmap_height+2*radius)]
    if smooth_flag:
        for detection in list_detections:
            for i in range(max(int(detection.center_x*scale)-radius, 0),
                        min(int(detection.center_x*scale)+radius+1, heatmap_width)):
                for j in range(max(int((detection.center_y+detection.height/2)*scale)-radius, 0),
                        min(int((detection.center_y+detection.height/2)*scale)+radius+1, heatmap_height)):
                    heatmap[i][j] += math.exp(-((int(detection.center_x*scale)-i)**2+(int((detection.center_y+detection.height/2)*scale)-j)**2)/variance)
    else:
        for detection in list_detections:
            heatmap[int(detection.center_x*scale)][int((detection.center_y+detection.height/2)*scale)] += 1
    return heatmap

def numpy2base64(img):
    pil_img = Image.fromarray(img)
    buff = BytesIO()
    pil_img.save(buff, format="JPEG")
    return base64.b64encode(buff.getvalue()).decode("utf-8")
