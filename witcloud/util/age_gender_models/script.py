import sys
sys.path.insert(0, 'python/')
import caffe
from caffe.proto import caffe_pb2

net_param = caffe_pb2.NetParameter()
net_str = open('age_net.caffemodel', 'r').read()
net_param.ParseFromString(net_str)

print (net_param.layer[0].name)  # first layer
print (net_param.layer[-1].name)  # last layer