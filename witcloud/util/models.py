import os
cpath = os.path.dirname(os.path.abspath(__file__))

# tracking models
def pbModel(): return os.path.join(cpath, "people_tracking_models/mars-small128.pb")

# age-gender models
# def ageGenderWeights(): return os.path.join(cpath, "age_gender_models/weights.28-3.73.hdf5")
def ageGenderProto(): return os.path.join(cpath, "age_gender_models/deploy.prototxt")
def ageGenderModel(): return os.path.join(cpath, "age_gender_models/res10_300x300_ssd_iter_140000.caffemodel")
def deployAgeProto(): return os.path.join(cpath, "age_gender_models/deploy_age.prototxt")
def ageNetModel(): return os.path.join(cpath, "age_gender_models/age_net.caffemodel")
def deployGenderProto(): return os.path.join(cpath, "age_gender_models/deploy_gender.prototxt")
def genderNetModel(): return os.path.join(cpath, "age_gender_models/gender_net.caffemodel")

# emotion-detector models
def emotionModel(): return os.path.join(cpath, "emotion_models/emotion_model.hdf5")
