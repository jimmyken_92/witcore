import os
cpath = os.path.dirname(os.path.abspath(__file__))

def haarcascade(LABEL):
	if LABEL == "BODY":
		return os.path.join(cpath, "haarcascade/haarcascade_fullbody.xml")

	if LABEL == "FACE":
		return os.path.join(cpath, "haarcascade/haarcascade_frontalface_default.xml")

	if LABEL == "EYES":
		return os.path.join(cpath, "haarcascade/haarcascade_eye.xml")

	if LABEL == "":
		return None
