import os
import re
import random
import string 
from dateutil import tz

def transform_coords(lista, num_images, w, h):
    import numpy as np
    assert num_images > 0, "num should be greater than zero"
    grid_len = np.sqrt(num_images)
    assert grid_len - int(grid_len) == 0, "number should be squared power"
    data = []
    for i in range(num_images):
        data.append([])

    for coor in range(len(lista)):
        lis_coor_news = list()
        x = lista[coor][2][0]
        y = lista[coor][2][1]
        n_x = x%w
        n_y = y%h
        r_x = int(x//w)
        r_y = int(y//h)
        pos = r_x * int(grid_len) + r_y 
        assert r_x < grid_len, "Wrong width size? r_x -> %d, grid_len -> %d"%(r_x, grid_len)
        assert r_y < grid_len, "Wrong height size?"
        assert pos >= 0, "Pos computed: %d"%pos
        assert pos < num_images, "Pos computed: %d"%pos
        r = (lista[coor][0],lista[coor][1],(n_x,n_y,lista[coor][2][2],lista[coor][2][3]))
        # (label, confidence, (x,y,w,h))
        data[pos].append(r)
    return data

def make_image_mosaic(list_images):
    import numpy as np
    assert len(list_images) > 0, "Len of list_images must be greater than zero"
    assert type(list_images[0])==type(np.zeros((1,1))), "Type of list_images elements must be np.array, %s provided"%str(type(list_images[0]))
    siz = int(np.sqrt(len(list_images)))
    assert siz*siz == len(list_images), "Len of list_images must be squared, %d provided"%len(list_images)

    mosaic = np.empty((0,list_images[0].shape[1]*siz, 3), dtype=np.uint8)
    for id in range(0,len(list_images), siz):
        row = np.hstack(tuple(list_images[id:id+siz]))
        mosaic = np.vstack((mosaic, row))
    return mosaic

def save_image(shot, name):
    import skimage.io
    skimage.io.imsave(name, shot)

def delete_file(name):
    import os
    if os.path.exists(name):
        os.remove(name)
    else:
        print("file not exists -->", name)

def read_image(name):
    import skimage.io
    return skimage.io.imread(name)

def filename_generator(TMP_FOLDER="/tmp", extension=".png"):
    import os
    import datetime
    assert os.path.exists(TMP_FOLDER), 'TMP_FOLDER does not exist yet. Run mount_ram_folder.sh script in appflx directory.'
    while(1):
        now = datetime.datetime.now()
        filename = "%s%s"%(now.strftime("%Y_%m_%d_%H_%M_%S_%f"),extension)
        path = os.path.join(TMP_FOLDER, filename)
        yield path

def check_is_running(name):
    import subprocess
    process = subprocess.Popen("ps aux | grep '%s'"%name,
                            shell=True,
                            stdout=subprocess.PIPE,
                        )
    (stdoutdata, stderrdata) = process.communicate()
    print("stderrdata -->", stderrdata)
    print("stdoutdata -->", stdoutdata)
    output = str(stdoutdata.decode('utf-8'))
    output = output.replace("grep %s"%name, "")
    output = output.replace("grep '%s'"%name, "")
    result = output.find(name)
    if result>0:
        return True
    return False

def kill_process_by_pid(pid):
    try:
        parent_pid = int(pid)
        parent = psutil.Process(parent_pid)
        for child in parent.children(recursive=True):  # or parent.children() for recursive=False
            child.kill()
        parent.kill()
        return True
    except:
        return False

def get_pids_by_cam_id(cam_id=0):
    import subprocess
    command = "ps aux | grep python3"
    result = subprocess.check_output(command, shell=True, bufsize=10**8)
    lines =  result.decode().split("\n")
    print(lines, '**************************************************************************************')
    success = True
    pid = 0
    for line in lines:
        cam_ids_found = re.findall(r"cam_id\":\s*(\d+).*", line)
        print(len(cam_ids_found), cam_ids_found)
        if len(cam_ids_found)>0:
            cam_id_found = cam_ids_found[0]
            print("cam_id_found", cam_id_found, "... ", cam_id)
            if int(cam_id_found)==int(cam_id):
                pids = re.findall("0xFFFFFF\s+(\d+)\s", line)
                print(pids, "found pid")
                return pids
        else:
            print("not pids found")
            return []

def code_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def get_camera_settings(cam_id, roi_ids, database, host_db, user_db, password_db):
    import mysql.connector
    # --------------  SQL CONNECTION  ------------------------- #
    cnx = mysql.connector.connect(user=user_db,
                        password=password_db,
                        host=host_db,
                        database=database)
    cursor = cnx.cursor(buffered=True)
    # --------------  SQL CONNECTION  ------------------------- #

    # - retrieving CAMERA PARAMS - #
    cam_fields = ['url', 'ip', 'user', 'pwd', 'enable_save_video', 'enable', 'sw_grab_image', 'max_resolution', 'mosaic_size', 'fps_grab_img', 'duration', 'detection_confidence', 'detection_max_distance', 'detection_ppf', 'schedule', 'count_staff', 'occupation_max', 'service_time_mins', 'open_time', 'close_time', 'motion_threshold', 'frame_history', 'frames_to_record', 'min_percent_area']

    cursor.execute("SELECT url, ip, user, pwd, enable_save_video, enable, sw_grab_image, max_resolution, mosaic_size, fps_grab_img, duration, detection_confidence, detection_max_distance, detection_ppf, schedule, count_staff, occupation_max, service_time_mins, open_time, close_time, motion_threshold, frame_history, frames_to_record, min_percent_area FROM cameras_camera WHERE cameras_camera.id=%s", (cam_id,))
    cam_resp = cursor.fetchone()
    camera_dict = None
    if cam_resp is not None:
        camera_dict = {}
        for index, field in enumerate(cam_fields):
            print(field, cam_resp[index])
            camera_dict[field] = cam_resp[index]
        
        # - retrieving witcloud admin settings ID PARAMS - #
        admin_fields = ['node_manager_url','listening_topic','callback_topic','gpu_url','endpoints_server','endpoints_version','endpoints_name','pubsub_server', 'notification_server']
        cursor.execute("SELECT node_manager_url,listening_topic,callback_topic,gpu_url,endpoints_server,endpoints_version,endpoints_name,pubsub_server,notification_server FROM superadmin_witcloudsettings")
        admin_resp = cursor.fetchone()
        for index, field in enumerate(admin_fields):
            print(field, admin_resp[index])
            camera_dict[field] = admin_resp[index]

        # - retrieving ROI ID PARAMS - #
        camera_dict['roi_list'] = []
        # - retrieving NOTIFICATIONS SETTINGS - #
        camera_dict['notifications'] = {}
        for roi_id in roi_ids:
            roi_fields = ['name', 'coordinates', 'type_roi_id', 'id_functionality_id']
            cursor.execute("SELECT name, coordinates, type_roi_id, id_functionality_id FROM roi_roi where roi_roi.id=%s", (roi_id,))
            roi_resp = cursor.fetchone()
            roi = {}
            roi['id'] = roi_id
            if roi_resp is not None:
                for index, field in enumerate(roi_fields):
                    # to retrieve NOTIFICATIONS SETTINGS #
                    if field=='id_functionality_id':
                        id_functionality_id = roi_resp[index]
                        cursor.execute("SELECT event_event.name,event_event.id FROM functionality_functionality_events INNER JOIN event_event ON functionality_functionality_events.id=%s", (id_functionality_id,))
                        event_name = cursor.fetchone()
                        if event_name is not None:
                            roi['event'] = event_name[0]
                            roi['event_id'] = event_name[1]
                        else:
                            roi['event'] = 'movement'
                            roi['event_id'] = 2
                        print("EVENT_NAME --", event_name)

                        print("id_functionality_id -> ", id_functionality_id)
                        cursor.execute("SELECT channel_channel.name,channel_channel.id FROM functionality_functionality_channels INNER JOIN channel_channel ON functionality_functionality_channels.id=%s", (id_functionality_id,))
                        channel_name = cursor.fetchone()
                        if channel_name is not None:
                            roi['channel'] = channel_name[0]
                            roi['channel_id'] = channel_name[1]
                        else:
                            roi['channel'] = 'whatsapp'
                            roi['channel_id'] = '595992245352' # my number
                        print("CHANNEL_NAME --", channel_name)
                        cursor.close()

                    print(field, roi_resp[index])
                    roi[field] = roi_resp[index]
                camera_dict['roi_list'].append(roi)

            # cursor.execute("SELECT roi_roi_id_functionality.functionality_id FROM roi_roi_id_functionality WHERE roi_roi_id_functionality.roi_id=%s", (roi_id,))
            # functionality_id = cursor.fetchone()
            # if functionality_id is None:
            #     print("This ROI roi_id[%d] does not have any functionality associated with "%roi_id)
            #     raise Exception("This ROI roi_id[%d] does not have any functionality associated with "%roi_id)
            #     # continue
            # # cursor.execute(" SELECT channel_channel.name FROM functionality_functionality_channels INNER JOIN channel_channel ON functionality_functionality_channels.id=%s", (channel_id,))
            # #asking for EVENT ID
            # cursor.execute("SELECT functionality_functionality_events.event_id FROM functionality_functionality_events WHERE functionality_functionality_events.functionality_id=%s", (functionality_id))
            # event_resp = cursor.fetchone()

            # #asking for CHANNEL ID
            # cursor.execute("SELECT functionality_functionality_channels.channel_id FROM functionality_functionality_channels WHERE functionality_functionality_channels.functionality_id=%s", (functionality_id))
            # channel_resp = cursor.fetchone()
            
            # if event_resp is None or channel_resp is None:
            #     print("This FUNCTIONALITY functionality_id[%d] does not have either event or channel setup " % functionality_id)
            #     continue

            # print("channel_resp !!!!!!!!!!!!!!", channel_resp, channel_resp[0])
            # channel_id = channel_resp[0]
            # print("channel_id -->", channel_id)
            # event_id = event_resp[0]
            # print("event_id -->", event_id)

            # #asking for EVENT NAME
            # cursor.execute("SELECT event_event.name FROM event_event WHERE event_event.id=%s", (event_id,))
            # event_resp = cursor.fetchone()
            # event_ = event_resp[0]

            # #asking for CHANNEL NAME
            # cursor.execute("SELECT channel_channel.name FROM channel_channel WHERE channel_channel.id=%s", (channel_id,))
            # ch_resp = cursor.fetchone()
            # channel_ = ch_resp[0]

            # if event_ not in camera_dict['notifications'].keys():
            #     print("adding a new event not included before ---")
            #     camera_dict['notifications'][event_] = []

            # if channel_ not in camera_dict['notifications'][event_]:
            #     print("adding a new channel [%s] for event type [%s] not included before ---" % (channel_, event_))
            #     camera_dict['notifications'][event_].append(channel_)

    return camera_dict

def pretty_print(d, indent=0):
    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty_print(value, indent+1)
        else:
            print('\t' * (indent+1) + str(value))

def inside_polygon(x, y, points):
    """
    Return True if a coordinate (x, y) is inside a polygon defined by
    a list of verticies [(x1, y1), (x2, x2), ... , (xN, yN)].

    Reference: http://www.ariel.com.au/a/python-point-int-poly.html
    """
    n = len(points)
    inside = False
    p1x, p1y = points[0]
    for i in range(1, n + 1):
        p2x, p2y = points[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x, p1y = p2x, p2y
    return inside

def upload_image_to_gcs_bucket(image_data, image_name, bucket_name):
    from google.cloud import storage
    from google.cloud.storage import Blob
    from io import BytesIO
    from PIL import Image
    client = storage.Client(project='witcloud')
    if client.lookup_bucket(bucket_name) is None:
        # create the new bucket
        bucket = client.create_bucket(bucket_name)
    else:
        bucket = client.get_bucket(bucket_name)
    blob = Blob(image_name, bucket)
    pil_img = Image.fromarray(image_data)
    buff = BytesIO()
    pil_img.save(buff, format="JPEG")
    blob.upload_from_string(buff.getvalue(), '/tmp/image/png')
    blob.make_public()
    return blob.public_url
    
def get_insert_stmt(data, table):
    """
        Data must be a valid python dict.
        Table a string.
        Return the insert statement.
        Then call:
            cursor.execute(stmt, list(data.values()))
            cnx.commit()
    """
    assert(type(data) is dict), "Object received is not a dict."
    placeholder = ", ".join(["%s"] * len(data))
    stmt = "insert into `{table}` ({columns}) values ({values});".format(table=table
                , columns=",".join(data.keys())
                , values=placeholder)
    return stmt

def check_in_schedule(camera_dict):
    import datetime
    now = datetime.datetime.now(tz=tz.gettz(camera_dict["timezone"]))
    weekday = now.weekday()
    current_ = now.strftime("%H:%M")
    periods = camera_dict["schedule"][str(weekday)]
    for period in periods:
        min_ = period[0]
        max_ = period[1]
        if min_ < current_ < max_:
            return True
    return False

def read_json_file(json_filepath):
    import json
    with open(json_filepath) as f:
        data = json.load(f)
    return data
