import numpy as np
import skimage.io
import datetime
import os

def transform_coords(lista, num_images, w, h):
    assert num_images > 0, "num should be greater than zero"
    grid_len = np.sqrt(num_images)
    assert grid_len - int(grid_len) == 0, "number should be squared power"
    data = []
    for i in range(num_images):
        data.append([])

    for coor in range(len(lista)):
        lis_coor_news = list()
        x = lista[coor][2][0]
        y = lista[coor][2][1]
        n_x = x%w
        n_y = y%h
        r_x = int(x/w)
        r_y = int(y/h)
        pos = r_x * int(grid_len) + r_y 
        assert r_x < grid_len, "Wrong width size?"
        assert r_y < grid_len, "Wrong height size?"
        assert pos >= 0, "Pos computed: %d"%pos
        assert pos < num_images, "Pos computed: %d"%pos
        r = (lista[coor][0],lista[coor][1],(n_x,n_y,lista[coor][2][2],lista[coor][2][3]))
        # (label, confidence, (x,y,w,h))
        data[pos].append(r)
    return data

def make_image_mosaic(list_images):
    assert len(list_images) > 0, "Len of list_images must be greater than zero"
    assert type(list_images[0])==type(np.zeros((1,1))), "Type of list_images elements must be np.array, %s provided"%str(type(list_images[0]))
    siz = int(np.sqrt(len(list_images)))
    assert siz*siz == len(list_images), "Len of list_images must be squared, %d provided"%len(list_images)

    mosaic = np.empty((0,list_images[0].shape[1]*siz, 3), dtype=np.uint8)
    for id in range(0,len(list_images), siz):
        row = np.hstack(tuple(list_images[id:id+siz]))
        mosaic = np.vstack((mosaic, row))
    return mosaic

def save_image(shot, name):
    skimage.io.imsave(name, shot)

def delete_file(name):
    if os.path.exists(name):
        os.remove(name)

def read_image(name):
    return skimage.io.imread(name)

def filename_generator(TMP_FOLDER="/tmp", extension=".png"):
    assert os.path.exists(TMP_FOLDER), 'TMP_FOLDER does not exist yet. Run mount_ram_folder.sh script in appflx directory.'
    while(1):
        now = datetime.datetime.now()
        filename = "%s%s"%(now.strftime("%Y_%m_%d_%H_%M_%S_%f"),extension)
        path = os.path.join(TMP_FOLDER, filename)
        yield path