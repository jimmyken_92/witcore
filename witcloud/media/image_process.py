import matplotlib.patches as patches
import numpy as np
import skimage.draw
# from skimage.draw import polygon_perimeter
import skimage.io as io

# https://github.com/scikit-image/scikit-image/issues/2688
def rectangle(r0, c0, width, height):
    rr, cc = [r0, r0 + width, r0 + width, r0], [c0, c0, c0 + height, c0 + height]
    return skimage.draw.polygon(rr, cc)

def rectangle_perimeter(c0, r0, width, height, shape=None, clip=False):
    rr, cc = [c0- height/2, c0- height/2, c0+ height/2, c0+ height/2], [r0- width/2, r0+ width/2, r0+ width/2, r0- width/2]
    return skimage.draw.polygon_perimeter(rr, cc, shape=shape, clip=clip)

def draw_rectangle(image, pos_x, pos_y, width, height, color=(0, 255, 0)):
    """
        color = (r, g, b)
    """
    rr, cc = rectangle_perimeter( pos_y, pos_x, width, height, shape=image.shape)
    image[rr, cc, :] = color
    rr, cc = rectangle_perimeter( pos_y, pos_x, width-1, height-1, shape=image.shape)
    image[rr, cc, :] = color
    rr, cc = rectangle_perimeter( pos_y, pos_x, width-2, height-2, shape=image.shape)
    image[rr, cc, :] = color
    return image

def save_image(filename, image):
    """
        inputs:
            filename
            image
    """
    io.imsave(filename, image)

def read_image(filename):
    return io.imread(filename)