import subprocess
import numpy as np
import platform
import subprocess as sp
import os
import skvideo.io
import time
from xmltodict import parse as xmltodictparser
import threading

def ffprobe(filename):
    """get metadata by using ffprobe

    Checks the output of ffprobe on the desired video
    file. MetaData is then parsed into a dictionary.

    Parameters
    ----------
    filename : string
        Path to the video file

    Returns
    -------
    metaDict : dict
       Dictionary containing all header-based information 
       about the passed-in source video.

    """
    # check if FFMPEG exists in the path
    # assert _HAS_FFMPEG, "Cannot find installation of real FFmpeg (which comes with ffprobe)."

    try:
        command = ["ffprobe", "-v", "error", "-show_streams", "-print_format", "xml", filename]

        ffprobe = subprocess.Popen(command, stderr=subprocess.PIPE,
                                  stdout=subprocess.PIPE)
        xml, err = ffprobe.communicate()
        if(err or not(len(xml))):
            print(err)
            return None

        d = xmltodictparser(xml)["ffprobe"]

        d = d["streams"]

        #import json
        #print json.dumps(d, indent = 4)
        #exit(0)

        # check type
        streamsbytype = {}
        if type(d["stream"]) is list:
            # go through streams
            for stream in d["stream"]:
                streamsbytype[stream["@codec_type"].lower()] = stream
        else:
            streamsbytype[d["stream"]["@codec_type"].lower()] = d["stream"]

        return streamsbytype
    except:
        return {}

def get_video_info(fileloc):
    """get video metadata about a video source [fileloc] by using ffprobe system command

    Checks the output of ffprobe on the desired video
    file. MetaData is then parsed into a dictionary.

    Parameters
    ----------
    fileloc : string
        Video source path. It could be a URL or a video file path valid

    Returns
    -------
    metaDict : dict
        Keys:
            file
            width
            height
            fps
            duration
    """
    command = ['ffprobe',
               '-v', 'fatal',
               '-show_entries', 'stream=width,height,r_frame_rate,duration',
               '-of', 'default=noprint_wrappers=1:nokey=1',
               fileloc, '-sexagesimal']

    ffmpeg = subprocess.Popen(command, stderr=subprocess.PIPE,
                              stdout=subprocess.PIPE)
    out, err = ffmpeg.communicate()
    if err or not len(out):
        print(err)
        return None
    if platform.system() == 'Windows':
        out = out.split(b'\r\n')
    else:
        out = out.split(b'\n')
    return {'file': fileloc,
            'width': int(out[0]),
            'height': int(out[1]),
            'fps': float(out[2].split(b'/')[0]) / float(out[2].split(b'/')[1]),
            'duration': out[3]}

def estimate_fps(video_url):
    """
        Method to compute current framerate provided by the video source `video_url`
        It grabs 5 frames from `video_url` and calculates the time elapsed.

    Parameters
    ----------
    video_url : string
        Path of video source

    Returns
    -------
    fps : float
        Framerate calculated in frames per second

    """
    reader = FFmpegReader(video_url)
    frame_generator = reader.frame_generator()

    ini = time.time()
    for i in range(5):
        frame_generator.next()
    reader.close()
    elapsed = time.time() - ini
    fps = 5/elapsed
    return fps

def get_video_frames(video_file, width, height):
    # command = ['.\\ffmpeg-win64\\bin\\ffmpeg',
    command = [ 'ffmpeg',
                # '-r', '25',
                # '-framerate', '1',
                # '-enc_time_base', '-1',
                '-i', video_file,
                # '-nostats',
                # '-hide_banner',
                # '-loglevel', '-8',
                '-f', 'image2pipe',
                '-vf', 'scale=' + str(width) + ':' + str(height),
                '-pix_fmt', 'rgb24',
                # '-preset:v', 'ultrafast',
                # '-movflags', '+faststart',
                '-vcodec', 'rawvideo', '-']
    print("command -", " ".join(command))
    pipe = subprocess.Popen(command, stdout=subprocess.PIPE, bufsize=10 ** 8)

    # throw away the data in the pipe's buffer.
    f = None
    framei = None
    framej = None
    masks = []
    i = 0
    none_count = 0
    while True:
        i = i + 1
        if none_count > 5:
            none_count=0
            pipe.kill()
            print("Restarting underlying process")
            pipe = subprocess.Popen(command, stdout=subprocess.PIPE, bufsize=10 ** 8)
        try:
            # read 420*360*3 bytes (= 1 frame)
            raw_image = pipe.stdout.read(width * height * 3)
            # transform the byte read into a numpy array
            if len(raw_image) == 0:
                print ("lenght of Raw Image is 0")
                none_count += 1
                continue
            image = np.fromstring(raw_image, dtype='uint8').reshape((height, width, 3))
            yield image
            pipe.stdout.flush()
        except Exception as e:
            print (e)
            break
    pipe.kill()

def create_images_videos(fileloc,start):
    fps = get_video_info(fileloc)['fps']
    basename = os.path.basename(fileloc)
    folder = '/tmp/'+basename.split('.')[0]+'/'
    os.mkdir(folder)
    os.system("ffmpeg -i "+ fileloc+" -f image2 "+folder+"%05d.png")
    lis_images = os.listdir(folder)
    lis_images_order = sorted(lis_images)
    fps_sum = int((1/fps)*1000)
    time_image = start
    lis_images_news = list()

    for image in lis_images_order:
        if image.endswith('.png'):
            os.rename(folder+image,folder+str(time_image)+'.png')
            lis_images_news.append(folder+str(time_image)+'.png')
            time_image += fps_sum    

class VideoWritter:
    def __init__(self, filename, inputdict=None, outputdict=None):
        self.filename = filename
        self.writter = skvideo.io.FFmpegWriter(self.filename, inputdict=inputdict, outputdict=outputdict)
        self.width = None
        self.height = None
        self.start = None
        self.end = None
        self.nframes = 0
        print("---------------------------------------") 
        print("------ NEW VIDEOWRITER CREATED  -------")
        print("-- VIDEO FILENAME: %s   -- ", self.filename)

    def writeFrame(self, timestamp_frame):
        timestamp, frame = timestamp_frame
        if self.start is None:
            self.start = timestamp
        self.end = timestamp
        self.writter.writeFrame(frame)
        self.nframes += 1
        assert type(frame) == type(np.zeros((1,1,1)))
        if self.width is None and self.height is None:
            self.height = frame.shape[0]
            self.width = frame.shape[1]
        else:
            assert frame.shape == ( self.height, self.width, 3), "All frame shapes should be equal to %s, provided %s"%(str((self.height, self.width, 3)), str(frame.shape))

    def close_video(self):
        self.writter.close()
        metadata = dict({
            "filename" : self.filename,
            "start": self.start,
            "end": self.end,
            "width": self.width,
            "height": self.height,
            "fps": self.nframes/(self.end-self.start),
            "duration": self.end-self.start
            })
        return metadata

class FFmpegReader(threading.Thread):
    """Reads frames using FFmpeg

    Using FFmpeg as a backend, this class
    provides sane initializations meant to
    handle the default case well.

    """
    def __init__(self, filename, inputdict=None, outputdict=None, verbosity=0):
        """Initializes FFmpeg in reading mode with the given parameters

        During initialization, additional parameters about the video file
        are parsed using :func:`skvideo.io.ffprobe`. Then FFmpeg is launched
        as a subprocess. Parameters passed into inputdict are parsed and
        used to set as internal variables about the video. If the parameter,
        such as "Height" is not found in the inputdict, it is found through
        scanning the file's header information. If not in the header, ffprobe
        is used to decode the file to determine the information. In the case
        that the information is not supplied and connot be inferred from the
        input file, a ValueError exception is thrown.

        Parameters
        ----------
        filename : string
            Video file path

        inputdict : dict
            Input dictionary parameters, i.e. how to interpret the input file.

        outputdict : dict
            Output dictionary parameters, i.e. how to encode the data 
            when sending back to the python process.

        Returns
        -------
        none

        """
        # check if FFMPEG exists in the path
        # assert _HAS_FFMPEG, "Cannot find installation of real FFmpeg (which comes with ffprobe)."


        bpplut = {}
        bpplut["yuv420p"] = [3, 12]
        bpplut["yuyv422"] = [3, 16]
        bpplut["rgb24"] = [3, 24]
        bpplut["bgr24"] = [3, 24]
        bpplut["yuv422p"] = [3, 16]
        bpplut["yuv444p"] = [3, 24]
        bpplut["yuv410p"] = [3, 9]
        bpplut["yuv411p"] = [3, 12]
        bpplut["gray"] = [1, 8]
        bpplut["monow"] = [1, 1]
        bpplut["monob"] = [1, 1]
        bpplut["pal8"] = [1, 8]
        bpplut["yuvj420p"] = [3, 12]
        bpplut["yuvj422p"] = [3, 16]
        bpplut["yuvj444p"] = [3, 24]
        bpplut["xvmcmc"] = [0, 0]
        bpplut["xvmcidct"] = [0, 0]
        bpplut["uyvy422"] = [3, 16]
        bpplut["uyyvyy411"] = [3, 12]
        bpplut["bgr8"] = [3, 8]
        bpplut["bgr4"] = [3, 4]
        bpplut["bgr4_byte"] = [3, 4]
        bpplut["rgb8"] = [3, 8]
        bpplut["rgb4"] = [3, 4]
        bpplut["rgb4_byte"] = [3, 4]
        bpplut["nv12"] = [3, 12]
        bpplut["nv21"] = [3, 12]
        bpplut["argb"] = [4, 32]
        bpplut["rgba"] = [4, 32]
        bpplut["abgr"] = [4, 32]
        bpplut["bgra"] = [4, 32]
        bpplut["gray16be"] = [1, 16]
        bpplut["gray16le"] = [1, 16]
        bpplut["yuv440p"] = [3, 16]
        bpplut["yuvj440p"] = [3, 16]
        bpplut["yuva420p"] = [4, 20]
        bpplut["vdpau_h264"] = [0, 0]
        bpplut["vdpau_mpeg1"] = [0, 0]
        bpplut["vdpau_mpeg2"] = [0, 0]
        bpplut["vdpau_wmv3"] = [0, 0]
        bpplut["vdpau_vc1"] = [0, 0]
        bpplut["rgb48be"] = [3, 48]
        bpplut["rgb48le"] = [3, 48]
        bpplut["rgb565be"] = [3, 16]
        bpplut["rgb565le"] = [3, 16]
        bpplut["rgb555be"] = [3, 15]
        bpplut["rgb555le"] = [3, 15]
        bpplut["bgr565be"] = [3, 16]
        bpplut["bgr565le"] = [3, 16]
        bpplut["bgr555be"] = [3, 15]
        bpplut["bgr555le"] = [3, 15]
        bpplut["vaapi_moco"] = [0, 0]
        bpplut["vaapi_idct"] = [0, 0]
        bpplut["vaapi_vld"] = [0, 0]
        bpplut["yuv420p16le"] = [3, 24]
        bpplut["yuv420p16be"] = [3, 24]
        bpplut["yuv422p16le"] = [3, 32]
        bpplut["yuv422p16be"] = [3, 32]
        bpplut["yuv444p16le"] = [3, 48]
        bpplut["yuv444p16be"] = [3, 48]
        bpplut["vdpau_mpeg4"] = [0, 0]
        bpplut["dxva2_vld"] = [0, 0]
        bpplut["rgb444le"] = [3, 12]
        bpplut["rgb444be"] = [3, 12]
        bpplut["bgr444le"] = [3, 12]
        bpplut["bgr444be"] = [3, 12]
        bpplut["ya8"] = [2, 16]
        bpplut["bgr48be"] = [3, 48]
        bpplut["bgr48le"] = [3, 48]
        bpplut["yuv420p9be"] = [3, 13]
        bpplut["yuv420p9le"] = [3, 13]
        bpplut["yuv420p10be"] = [3, 15]
        bpplut["yuv420p10le"] = [3, 15]
        bpplut["yuv422p10be"] = [3, 20]
        bpplut["yuv422p10le"] = [3, 20]
        bpplut["yuv444p9be"] = [3, 27]
        bpplut["yuv444p9le"] = [3, 27]
        bpplut["yuv444p10be"] = [3, 30]
        bpplut["yuv444p10le"] = [3, 30]
        bpplut["yuv422p9be"] = [3, 18]
        bpplut["yuv422p9le"] = [3, 18]
        bpplut["vda_vld"] = [0, 0]
        bpplut["gbrp"] = [3, 24]
        bpplut["gbrp9be"] = [3, 27]
        bpplut["gbrp9le"] = [3, 27]
        bpplut["gbrp10be"] = [3, 30]
        bpplut["gbrp10le"] = [3, 30]
        bpplut["gbrp16be"] = [3, 48]
        bpplut["gbrp16le"] = [3, 48]
        bpplut["yuva420p9be"] = [4, 22]
        bpplut["yuva420p9le"] = [4, 22]
        bpplut["yuva422p9be"] = [4, 27]
        bpplut["yuva422p9le"] = [4, 27]
        bpplut["yuva444p9be"] = [4, 36]
        bpplut["yuva444p9le"] = [4, 36]
        bpplut["yuva420p10be"] = [4, 25]
        bpplut["yuva420p10le"] = [4, 25]
        bpplut["yuva422p10be"] = [4, 30]
        bpplut["yuva422p10le"] = [4, 30]
        bpplut["yuva444p10be"] = [4, 40]
        bpplut["yuva444p10le"] = [4, 40]
        bpplut["yuva420p16be"] = [4, 40]
        bpplut["yuva420p16le"] = [4, 40]
        bpplut["yuva422p16be"] = [4, 48]
        bpplut["yuva422p16le"] = [4, 48]
        bpplut["yuva444p16be"] = [4, 64]
        bpplut["yuva444p16le"] = [4, 64]
        bpplut["vdpau"] = [0, 0]
        bpplut["xyz12le"] = [3, 36]
        bpplut["xyz12be"] = [3, 36]
        bpplut["nv16"] = [3, 16]
        bpplut["nv20le"] = [3, 20]
        bpplut["nv20be"] = [3, 20]
        bpplut["yvyu422"] = [3, 16]
        bpplut["vda"] = [0, 0]
        bpplut["ya16be"] = [2, 32]
        bpplut["ya16le"] = [2, 32]
        bpplut["qsv"] = [0, 0]
        bpplut["mmal"] = [0, 0]
        bpplut["d3d11va_vld"] = [0, 0]
        bpplut["rgba64be"] = [4, 64]
        bpplut["rgba64le"] = [4, 64]
        bpplut["bgra64be"] = [4, 64]
        bpplut["bgra64le"] = [4, 64]
        bpplut["0rgb"] = [3, 24]
        bpplut["rgb0"] = [3, 24]
        bpplut["0bgr"] = [3, 24]
        bpplut["bgr0"] = [3, 24]
        bpplut["yuva444p"] = [4, 32]
        bpplut["yuva422p"] = [4, 24]
        bpplut["yuv420p12be"] = [3, 18]
        bpplut["yuv420p12le"] = [3, 18]
        bpplut["yuv420p14be"] = [3, 21]
        bpplut["yuv420p14le"] = [3, 21]
        bpplut["yuv422p12be"] = [3, 24]
        bpplut["yuv422p12le"] = [3, 24]
        bpplut["yuv422p14be"] = [3, 28]
        bpplut["yuv422p14le"] = [3, 28]
        bpplut["yuv444p12be"] = [3, 36]
        bpplut["yuv444p12le"] = [3, 36]
        bpplut["yuv444p14be"] = [3, 42]
        bpplut["yuv444p14le"] = [3, 42]
        bpplut["gbrp12be"] = [3, 36]
        bpplut["gbrp12le"] = [3, 36]
        bpplut["gbrp14be"] = [3, 42]
        bpplut["gbrp14le"] = [3, 42]
        bpplut["gbrap"] = [4, 32]
        bpplut["gbrap16be"] = [4, 64]
        bpplut["gbrap16le"] = [4, 64]
        bpplut["yuvj411p"] = [3, 12]
        bpplut["bayer_bggr8"] = [3, 8]
        bpplut["bayer_rggb8"] = [3, 8]
        bpplut["bayer_gbrg8"] = [3, 8]
        bpplut["bayer_grbg8"] = [3, 8]
        bpplut["bayer_bggr16le"] = [3, 16]
        bpplut["bayer_bggr16be"] = [3, 16]
        bpplut["bayer_rggb16le"] = [3, 16]
        bpplut["bayer_rggb16be"] = [3, 16]
        bpplut["bayer_gbrg16le"] = [3, 16]
        bpplut["bayer_gbrg16be"] = [3, 16]
        bpplut["bayer_grbg16le"] = [3, 16]
        bpplut["bayer_grbg16be"] = [3, 16]
        bpplut["yuv440p10le"] = [3, 20]
        bpplut["yuv440p10be"] = [3, 20]
        bpplut["yuv440p12le"] = [3, 24]
        bpplut["yuv440p12be"] = [3, 24]
        bpplut["ayuv64le"] = [4, 64]
        bpplut["ayuv64be"] = [4, 64]
        bpplut["videotoolbox_vld"] = [0, 0]

        israw = 0

        threading.Thread.__init__(self)

        if not inputdict:
            inputdict = {}

        if not outputdict:
            outputdict = {}

        # General information
        # _, self.extension = os.path.splitext(filename)

        # smartphone video data is weird
        self.rotationAngle = '0'

        # self.size = os.path.getsize(filename)
        self.probeInfo = ffprobe(filename)

        viddict = {}
        if "video" in self.probeInfo:
            viddict = self.probeInfo["video"]

        self.inputfps = -1
        if ("-r" in inputdict):
            self.inputfps = np.int(inputdict["-r"])
        elif "@r_frame_rate" in viddict:
            # check for the slash
            frtxt = viddict["@r_frame_rate"]
            parts = frtxt.split('/')
            if len(parts) > 1:
                self.inputfps = np.float(parts[0])/np.float(parts[1])
            else:
                self.inputfps = np.float(frtxt)
        else:
            # simply default to a common 25 fps and warn
            self.inputfps = 25
            # No input frame rate detected. Assuming 25 fps. Consult documentation on I/O if this is not desired.

        # check for transposition tag
        if ('tag' in viddict):
          tagdata = viddict['tag']
          if not isinstance(tagdata, list):
            tagdata = [tagdata]

          for tags in tagdata:
            if tags['@key'] == 'rotate':
              self.rotationAngle = tags['@value']

        # if we don't have width or height at all, raise exception
        if ("-s" in inputdict):
            widthheight = inputdict["-s"].split('x')
            self.inputwidth = np.int(widthheight[0])
            self.inputheight = np.int(widthheight[1])
        elif (("@width" in viddict) and ("@height" in viddict)):
            self.inputwidth = np.int(viddict["@width"])
            self.inputheight = np.int(viddict["@height"])
        else:
            raise ValueError("No way to determine width or height from video. Need `-s` in `inputdict`. Consult documentation on I/O.")

        # smartphone recordings seem to store data about rotations
        # in tag format. Just swap the width and height
        if self.rotationAngle == '90' or self.rotationAngle == '270':
          self.inputwidth, self.inputheight = self.inputheight, self.inputwidth

        self.bpp = -1 # bits per pixel
        self.pix_fmt = ""
        self.verbosity = verbosity
        # completely unsure of this:
        if ("-pix_fmt" in inputdict):
            self.pix_fmt = inputdict["-pix_fmt"]
        elif ("@pix_fmt" in viddict):
            # parse this bpp
            self.pix_fmt = viddict["@pix_fmt"]
        else:
            self.pix_fmt = "yuvj444p"
            if verbosity != 0:
                warnings.warn("No input color space detected. Assuming yuvj420p.", UserWarning)

        self.inputdepth = np.int(bpplut[self.pix_fmt][0])
        self.bpp = np.int(bpplut[self.pix_fmt][1])

        # if (str.encode(self.extension) in [b".raw", b".yuv"]):
        #     israw = 1

        if ("-vframes" in outputdict):
            self.inputframenum = np.int(outputdict["-vframes"])
        elif ("@nb_frames" in viddict):
            self.inputframenum = np.int(viddict["@nb_frames"])
        # elif israw == 1:
        #     # we can compute it based on the input size and color space
        #     self.inputframenum = np.int(self.size / (self.inputwidth * self.inputheight * (self.bpp/8.0)))
        else:
            self.inputframenum = -1
            if verbosity != 0:
                warnings.warn("Cannot determine frame count. Scanning input file, this is slow when repeated many times. Need `-vframes` in inputdict. Consult documentation on I/O.", UserWarning) 

        # if israw != 0:
        #     inputdict['-pix_fmt'] = self.pix_fmt
        # else:
        #     # check that the extension makes sense
        #     assert str.encode(self.extension).lower() in _FFMPEG_SUPPORTED_DECODERS, "Unknown decoder extension: " + self.extension.lower()

        self._filename = filename

        if '-f' not in outputdict:
            outputdict['-f'] = "image2pipe"

        if '-pix_fmt' not in outputdict:
            outputdict['-pix_fmt'] = "rgb24"

        if '-s' in outputdict:
            widthheight = outputdict["-s"].split('x')
            self.outputwidth = np.int(widthheight[0])
            self.outputheight = np.int(widthheight[1])
        else:
            self.outputwidth = self.inputwidth
            self.outputheight = self.inputheight


        self.outputdepth = np.int(bpplut[outputdict['-pix_fmt']][0])
        self.outputbpp = np.int(bpplut[outputdict['-pix_fmt']][1])

        if '-vcodec' not in outputdict:
            outputdict['-vcodec'] = "rawvideo"

        # Create input args
        iargs = []
        for key in inputdict.keys():
            iargs.append(key)
            iargs.append(inputdict[key])

        oargs = []
        for key in outputdict.keys():
            oargs.append(key)
            oargs.append(outputdict[key])

        # if self.inputframenum == -1:
        #     # open process with supplied arguments,
        #     # grabbing number of frames using ffprobe
        #     probecmd = [_FFMPEG_PATH + "/ffprobe"] + ["-v", "error", "-count_frames", "-select_streams", "v:0", "-show_entries", "stream=nb_read_frames", "-of", "default=nokey=1:noprint_wrappers=1", self._filename]
        #     self.inputframenum = np.int(check_output(probecmd).decode().split('\n')[0])

        # Create process


        if verbosity == 0:
            self.cmd = ["ffmpeg", "-nostats", "-loglevel", "0"] + iargs + ['-i', self._filename] + oargs + ['-']
            self._proc = sp.Popen(self.cmd, stdin=sp.PIPE,
                                  stdout=sp.PIPE, stderr=sp.PIPE)
        else:
            self.cmd = ["ffmpeg"] + iargs + ['-i', self._filename] + oargs + ['-']
            print(self.cmd)
            self._proc = sp.Popen(self.cmd, stdin=sp.PIPE,
                                  stdout=sp.PIPE, stderr=None)

        print("----------------------------------------") 
        print("------ NEW VIDEO READER CREATED  -------")
        print("-- VIDEO SOURCE: %s   -- ", self._filename)

    # def run(self):
    #     while (1):
    #         time.sleep(0.1)
    #         self.last_timestamp, self.last_frame = self._read_frame()

    def getFrameSize(self):
        return (self.outputwidth, self.outputheight)


    def getShape(self):
        """Returns a tuple (T, M, N, C) 
        
        Returns the video shape in number of frames, height, width, and channels per pixel.
        """

        return self.inputframenum, self.outputheight, self.outputwidth, self.outputdepth


    def close(self):
        if self._proc is not None and self._proc.poll() is None:
            self._proc.stdin.close()
            self._proc.stdout.close()
            self._proc.stderr.close()
            self._terminate(0.2)
        self._proc = None

    def _terminate(self, timeout=1.0):
        """ Terminate the sub process.
        """
        # Check
        if self._proc is None:  # pragma: no cover
            return  # no process
        if self._proc.poll() is not None:
            return  # process already dead
        # Terminate process
        self._proc.terminate()
        # Wait for it to close (but do not get stuck)
        etime = time.time() + timeout
        while time.time() < etime:
            time.sleep(0.01)
            if self._proc.poll() is not None:
                break
    
    def _restartProcess(self):
        if self.verbosity == 0:
            self._proc = sp.Popen(self.cmd, stdin=sp.PIPE,
                                stdout=sp.PIPE, stderr=sp.PIPE)
        else:
            self._proc = sp.Popen(self.cmd, stdin=sp.PIPE,
                                stdout=sp.PIPE, stderr=None)
        time.sleep(0.2)

    def getFps(self):
        # TODO: Add fps computation...
        return estimate_fps(self._filename)
        
    def _read_frame_data(self):
        # Init and check
        framesize = self.outputdepth * self.outputwidth * self.outputheight
        assert self._proc is not None
        none_count = 0

        while(1):
            try:
                # Read framesize bytes
                raw_image = self._proc.stdout.read(framesize)

                if len(raw_image) == 0:
                    raise RuntimeError("lenght of Raw Image is 0")
                
                arr = np.fromstring(raw_image, dtype=np.uint8)

                assert len(arr) == framesize
                return arr
            except Exception as err:
                print(str(err))
                if none_count > 5:
                    none_count = 0
                    self._terminate()
                    print("Restarting underlying process")
                    self._restartProcess()
                else:
                    none_count += 1
                continue
        return arr

    # def nextFrame(self):
    #     return self.last_timestamp, self.last_frame

    def nextFrame(self):
        # Read and convert to numpy array
        # t0 = time.time()
        s = self._read_frame_data()
        ts = time.time()
        result = np.fromstring(s, dtype='uint8')

        result = result.reshape((self.outputheight, self.outputwidth, self.outputdepth))

        self._lastread = result
        return (ts,result)

    def frame_generator(self):
        """Yields frames using a generator 
        
        Returns T ndarrays of size (M, N, C), where T is number of frames, 
        M is height, N is width, and C is number of channels per pixel.

        """
        # for i in range(self.inputframenum):
        while 1:
            yield self.nextFrame()
