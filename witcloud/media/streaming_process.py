import ffmpeg
import util
import sys
import os
import time
cpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(cpath, '..'))

from detection import yolo as yolo
import image_process

def analize_video_and_write(video_source, duration=30, mosaic_size=9):
    list_frames = []
    net = yolo.load_witcloud_net()
    meta = yolo.load_witcloud_meta()

    fps = ffmpeg.estimate_fps(video_source)
    videoname = util.filename_generator(extension='.mp4').next()
    writter = ffmpeg.VideoWritter(videoname, inputdict={'-r':str(fps)})

    reader = ffmpeg.FFmpegReader(video_source)
    source = reader.frame_generator()
    init_time = time.time()
    finish_time = init_time + duration
    while time.time() < finish_time:
        frame = source.next()
        if len(list_frames) == mosaic_size:
            mosaic = util.make_image_mosaic(list_frames)
            file_name = util.filename_generator().next()
            util.save_image(mosaic, file_name)
            coords = yolo.detect(net, meta, file_name)
            # print(coords, list_frames[0][1].shape )
            try:
                coords_transf = util.transform_coords(lista=coords, num_images=mosaic_size, w=list_frames[0].shape[1], h=list_frames[0].shape[0])
            except Exception as e:
                print e, coords
                print (mosaic_size)
                print (list_frames[0].shape)
                raise e

            # draw boxes
            for j, coo in enumerate(coords_transf):
                for c in coo:
                    frame = image_process.draw_rectangle(list_frames[j], c[2][0], c[2][1], c[2][2], c[2][3])
                    list_frames[j] = frame
                writter.write(frame)
            list_frames = []

        else:
            list_frames.append(frame)
    writter.close_video()

def grab_and_analize_video_from_url(cam, yolo_detector, video_labelled_queue, video_original_queue, queue_coords):

    video_info = ffmpeg.get_video_info(cam.url)

    list_frames = []
    list_times = []
    nframes = 0
    writter_labelled = None
    writter_original = None
    start = None

    MAX_RESOLUTION = cam.MAX_RESOLUTION
    MAX_WIDTH = int(MAX_RESOLUTION.split('x')[0])
    MAX_HEIGHT = int(MAX_RESOLUTION.split('x')[1])
    width = video_info["width"]
    height = video_info["height"]
    # DETERMINE THE MAX RESOLUTION TO USE
    end_width = min(width, MAX_WIDTH)
    end_height = min(height, MAX_HEIGHT)


    fps = ffmpeg.estimate_fps(cam.url)

    reader = ffmpeg.FFmpegReader(cam.url)
    frame_generator = reader.frame_generator()

    finish_time = time.time() - cam.duration
    while 1:
        if time.time() > finish_time:
            end = time.time()
            if start is not None:
                data = dict({
                    "system_id": cam.system_id,
                    "center_id": cam.center_id,
                    "node_id":   cam.node_id,
                    "cam_id": cam.cam_id,
                    "start": start,
                    "end": end,
                    "duration": end-start,
                    "width": end_width,
                    "height": end_height,
                    "fps": nframes/(end-start),
                    "analyze_flag": False
                    })
                time.sleep(0.3)
                writter_labelled.close_video()
                video_labelled_queue.put((writter_labelled.filename, data))
                time.sleep(0.3)
                writter_original.close_video()
                video_original_queue.put((writter_original.filename, data))
                print("size of labelled video queue -> ", video_labelled_queue.qsize())
                print("size of original video queue -> ", video_original_queue.qsize())

            vname_labelled = util.filename_generator(extension='.mp4').next()
            vname_original = util.filename_generator(extension='.mp4').next()
            writter_labelled = ffmpeg.VideoWritter(vname_labelled, inputdict={'-r':str(fps)})
            writter_original = ffmpeg.VideoWritter(vname_original, inputdict={'-r':str(fps)})
            nframes = 0
            start = time.time()
            finish_time = start + cam.duration


        frame = frame_generator.next()
        now = time.time()
        writter_original.write(frame)
        nframes += 1
        if len(list_frames) == cam.mosaic_size:
            mosaic = util.make_image_mosaic(list_frames)
            # mosaic_queue.put(mosa)
            coords = yolo_detector(mosaic)
            # print(coords, list_frames[0][1].shape )
            try:
                coords_transf = util.transform_coords(lista=coords, num_images=cam.mosaic_size, w=list_frames[0].shape[1], h=list_frames[0].shape[0])
                for i, coord in enumerate(coords_transf):
                    if len(coord)!=0:
                        queue_coords.put((list_times[i], coord))
                        print("size of queues -> ", queue_coords.qsize())
            except Exception as e:
                print e, coords
                print (cam.mosaic_size)
                print (list_frames[0].shape)
                raise e

            # draw boxes
            for j, coo in enumerate(coords_transf):
                for c in coo:
                    frame = image_process.draw_rectangle(list_frames[j], c[2][0], c[2][1], c[2][2], c[2][3])
                    list_frames[j] = frame
                writter_labelled.write(frame)
            list_frames = []
            list_times = []

        else:
            list_frames.append(frame)
            list_times.append(now)
