import ffmpeg
import util
import sys
import os
cpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(cpath, '..'))

from detection import yolo
import image_process

def analize_video(video_file, mosaic_size=9):
    video_info = ffmpeg.get_video_info(video_file)

    list_frames = []
    net = yolo.load_witcloud_net()
    meta = yolo.load_witcloud_meta()

    fps = ffmpeg.estimate_fps(video_file)
    videoname = util.filename_generator(extension='.mp4').next()
    writter = ffmpeg.VideoWritter(videoname, inputdict={'-r':str(fps)})

    basename = os.path.basename(video_file)
    writter = ffmpeg.VideoWritter(video_file.replace(basename, "witcloud_%s"%(basename)), inputdict={'-r':str(fps)})
    
    reader = ffmpeg.FFmpegReader(video_file)
    source = reader.frame_generator()
    for frame in source.next():
        if len(list_frames) == mosaic_size:
            mosaic = util.make_image_mosaic(list_frames)
            file_name = util.filename_generator().next()
            util.save_image(mosaic, file_name)
            coords = yolo.detect(net, meta, file_name)
            # print(coords, list_frames[0][1].shape )
            try:
                coords_transf = util.transform_coords(lista=coords, num_images=mosaic_size, w=list_frames[0].shape[1], h=list_frames[0].shape[0])
            except Exception as e:
                print e, coords
                print (mosaic_size)
                print (list_frames[0].shape)
                raise e

            # draw boxes
            for j, coo in enumerate(coords_transf):
                for c in coo:
                    frame = image_process.draw_rectangle(list_frames[j], c[2][0], c[2][1], c[2][2], c[2][3])
                    list_frames[j] = frame
                writter.write(frame)
            list_frames = []

        else:
            list_frames.append(frame)
    writter.close_video()
