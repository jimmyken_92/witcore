class Frame(object):
    def __init__(self, data, data_gray, timestamp, id=0, id_video='0'):
        self.data = data
        self.data_gray = data_gray
        self.timestamp = timestamp
        self.id = id
        self.id_video = id_video
        self.width = data.shape[1]
        self.height = data.shape[0]
