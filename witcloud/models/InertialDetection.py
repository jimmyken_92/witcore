class InertialDetection(object):
    def __init__(self, x, y, width, height, timestamp, mx=0, my=0):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.mx = mx
        self.my = my
        self.timestamp = timestamp
