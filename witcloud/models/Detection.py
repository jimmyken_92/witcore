class Detection(object):
    # def __init__(self, label, confidence, center_x, center_y, width, height, timestamp, id_frame, id_video, roi_id=0):
    def __init__(self, dictionary):
        '''timestamp: units in milliseconds'''
        self.label = dictionary["label"]
        self.confidence = dictionary["confidence"]
        self.x1 = dictionary["x1"]
        self.x2 = dictionary["x2"]
        self.y1 = dictionary["y1"]
        self.y2 = dictionary["y2"]
        self.timestamp = dictionary["timestamp"]
        self.id_frame = dictionary["id_frame"]
        self.id_video = dictionary["id_video"]
        self.roi_id = dictionary["roi_id"]

    @property
    def toplef(self):
        ''' top left '''
        return (int(self.center_x - self.width // 2), int(self.center_y - self.height // 2))

    @property
    def botrig(self):
        ''' bottom right '''
        return (int(self.center_x + self.width // 2), int(self.center_y + self.height // 2))

    @property
    def x_base(self):
        return self.center_x

    @property
    def y_base(self):
        return self.center_y + self.height / 2
