import numpy as np
import time

class RealTimeMetricsGrouping(object):
    """
        Params:
        ======
            cam_id : int
            roi_id : int
            center_id: int
            zone : str
    """
    def __init__(self, roi_id, zone, zone_type):
        self.roi_id = roi_id
        self.zone = zone
        self.zone_type = zone_type
        self._set_init_values()

    def _set_init_values(self):
        self.timestamp = time.time()*1000
        self.visitors = []
        self.max_visits = 0
        self.min_visits = 0
        self.mean_visits = 0
        self.median_visits = 0
        self.records = 0
        self.duration = 0

    def add(self, timestamp, visitors):
        self.update_duration(timestamp)
        self.visitors.append(visitors)
        self.records +=1
        # print("len of visitors --->", len(self.visitors))

    def get_body_as_dict(self):
        # call to _compute_kpi() and reset values
        self._compute_kpi()
        # self.__dict__
        body = {
            "roi_id": self.roi_id,
            "zone": self.zone,
            "zone_type": self.zone_type,
            "timestamp": int(self.timestamp),
            "max_visits": int(self.max_visits),
            "min_visits": int(self.min_visits),
            "mean_visits": round(float(self.mean_visits),2),
            "median_visits": round(float(self.median_visits),2),
            "records": int(self.records),
            "duration": int(self.duration/1000) # to convert to seconds
        }
        # reset values to zero
        self._set_init_values()
        # print("returning body---- grtm", body)
        return body

    def _compute_kpi(self):
        self.update_duration(time.time()*1000)
        aux = [x  for x in self.visitors if x is not None]
        self.max_visits = 0
        self.min_visits = 0
        self.mean_visits = 0
        self.median_visits = 0
        # compute mean, max, min, avg
        if len(aux)>0:
            self.max_visits = max(aux)
            self.min_visits = min(aux)
            self.mean_visits = np.average(aux)
            self.median_visits = np.median(aux)

    def update_duration(self, timestamp):
        if self.timestamp is not None:
            self.duration = timestamp - self.timestamp
