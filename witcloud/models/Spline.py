from witcloud.models.InertialDetection import InertialDetection

class Spline(object):
    def __init__(self, id):
        self.id = id
        self.coordinates = []

    def add_node(self, inertial_detection:InertialDetection):
        self.coordinates.append(inertial_detection)
