class Camera(object):
    def __init__(self, dictionary):
        self.center_id = dictionary["center_id"]
        self.center = dictionary["center"]
        self.node_id = dictionary["node_id"]
        self.cam_id = dictionary["cam_id"]
        self.system_id = dictionary["system_id"]

        self.url = dictionary["url"]
        self.destination = dictionary["destination"]
        self.sw_grab_image = dictionary["sw_grab_image"]
        self.duration = dictionary["duration"]
        self.fps_grab_img = dictionary["fps_grab_img"]
        self.MAX_RESOLUTION = dictionary["MAX_RESOLUTION"]

        self.detection_max_distance = dictionary["detection_max_distance"]
        self.detection_ppf = dictionary["detection_ppf"]
        self.labels = dictionary["labels"]
        self.detection_confidence = dictionary["detection_confidence"]
        self.gpu_url = dictionary["gpu_url"]
        self.mosaic_size = dictionary["mosaic_size"]
        self.detection_file = dictionary["detection_file"]
        
        self.timezone = dictionary["timezone"]
        self.schedule = dictionary["schedule"]

        self.api_server = dictionary["api_server"]
        self.api_version = dictionary["api_version"]
        self.api_name = dictionary["api_name"]

        self.enable_save_video = dictionary["enable_save_video"]
        self.frame_history = dictionary["frame_history"]
        self.frames_to_record = dictionary["frames_to_record"]
        self.min_percent_area = dictionary["min_percent_area"]
        self.motion_threshold = dictionary["motion_threshold"]

        self.enable_roi = dictionary["enable_roi"]
        self.roi_list = dictionary["roi_list"]

        self.notification_server = dictionary["notification_server"]
        self.alerts = dictionary["alerts"]

    @property
    def is_ip_camera(self):
        return  '://' in self.url
