import numpy as np

class Roi(object):
    def __init__(self, xywh, counting_lines, vehicles_to_track, directions_to_track, id):
        """
        Params:
        -------
            xywh : tuple
            counting_lines : dict with keys directions=["down" [, "up", "right", "left"]]. Inside one item with key: "line".
            vehicles_to_track : a list of strings. ["car", "bus", "truck"]
            directions_to_track : a list of strings. Directions included in the counting_lines dict above.
        """
        self.id = id
        self.xywh = np.array(xywh)
        self.counting_lines = counting_lines # as a dict
        self.vehicles_to_track = vehicles_to_track #as a list of strings
        self.directions_to_track = directions_to_track #as a list of strings

    def _to_xlbr(self):
        self.xlbr = np.array(self.xywh)
        self.xlbr[2:] = self.xywh[:2]+self.xywh[2:]
        return self.xlbr
