import sys
import os
cpath = os.path.dirname(os.path.abspath(__file__))
from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.modules.CameraReader import CameraReader
from witcloud.models.Frame import Frame
from witcloud.models.Detection import Detection
from witcloud.util import util
import threading
import base64
import json
import requests
import uuid
import multiprocessing
import time

class Detector(WitcloudComponent, threading.Thread):
    '''Usage as a thread: calling the start() method after instantiation'''
    def __init__(self, roi={}, label='person', mosaic_size=9, api_detect_server="http://35.205.222.117:8080/image", detection_confidence=0.5, roi_list=None):
        WitcloudComponent.__init__(self, witcloudComponentName='detector'+str(time.time()))
        threading.Thread.__init__(self, name='detector'+str(time.time()))
        self.list_mosaic = []
        self.label = label
        self.mosaic_size = mosaic_size
        self.detection_confidence = detection_confidence
        self.api_detect_server = api_detect_server
        self.flag = True
        self.buffer = multiprocessing.Queue(maxsize=200)
        self.subscribers_detections_and_frames = []
        # to store frame cropping if there is
        assert type(roi)==dict, 'Roi is not a dict : '+roi
        if 'coordinates' in roi:
            self.roi = roi
        else:
            self.roi = None

    def register_detections_and_frames(self, subscriber):
        self.register(subscriber, self.subscribers_detections_and_frames)

    def run(self):
        print("Yolo Started")
        while self.flag:
            try:
                newFrame = self.buffer.get() #waits for a new element
                self.consume(newFrame)
            except Exception as e:
                print("ERROR while getting new frame from buffer %s " % str(e))
                self.errors.append("noFramesInBuffer")
                time.sleep(0.1)
            time.sleep(0.01)
                    
    def update(self, frame:Frame):
        #TODO limit size of buffer/override old elements
        if self.roi is not None:
            #doing cropping
            x, y, w, h = self.roi['coordinates']
            cropped = frame.data.copy()
            frame = Frame(id=frame.id, data=cropped[y:y+h, x:x+w, :], timestamp=frame.timestamp)
        try:
            self.buffer.put(frame)
        except Exception as e:
            print(e)
            self.buffer.get() #lost a frame
            self.buffer.put(frame)

    def consume(self, frame:Frame):
        if len(self.list_mosaic) == self.mosaic_size:
            print("Pushing mosaic into Detector Server")
            self.detect(self.list_mosaic)
            self.list_mosaic = []
        else:
            self.list_mosaic.append(frame)

    def _send_gpu_request(self, url, body,files):
        try:
            data = requests.post(self.api_detect_server, body,files=files)
            return json.loads(data.content.decode('utf-8'))
        except Exception as e:
            self.errors.append("Error sending to gpu ---> %s"%str(e))
        # print("data.content -> ", data.content)
        return None

    def _detect(self,mosaic, thresh=.35, hier_thresh=.5, nms=.45):
        filename = util.filename_generator(TMP_FOLDER="/tmp").__next__()
        util.save_image(mosaic, filename)
        # making request to GPU yolo
        files = {
          "image" : open(filename,'rb')
        }
        body = {
            "name_image": uuid.uuid4().hex, 
            "thresh": thresh,
            "hier_thresh": hier_thresh, 
            "nms": nms, 
        }
        print("Sending request to Detector Server -> %s"%self.api_detect_server)
        # data = requests.post(self.api_detect_server, body)
        data = self._send_gpu_request(self.api_detect_server, body,files=files)
        # print("type content", type(content))
        try:
            util.delete_file(filename)
        except Exception as e:
            self.errors.append("DETECTOR:ERROR while deleting temporal mosaic image -->%s"%str(e))
            print("Error while deleting temporal mosaic image -->%s"%str(e))
        
        if data is not None:
            # print("data received -> ", data[0])
            print("data received from Detector server -> %s "%str(data))
        else:
            self.errors.append("DETECTOR:ERROR no data received from Detector server ")
            print("no data received from Detector server ")
        return data

    def detect(self, list_frames: "list of Frame"):
        ''' Notify detections '''
        if len(list_frames)>0:
            print("len of list_frames %s"%str(len(list_frames)))
            # create a mosaic
            mosaic = util.make_image_mosaic([frame.data for frame in list_frames])
            data = self._detect(mosaic,thresh=self.detection_confidence)
            if data is not None:
                # filter label
                data_filtered = [d for d in data if d[0]==self.label]
                print("label ---> %s"%str(data_filtered))
                # now we go back from mosaic
                coords_transf = util.transform_coords(lista=data_filtered,
                                                    num_images=self.mosaic_size,
                                                    w=list_frames[0].data.shape[1],
                                                    h=list_frames[0].data.shape[0])

                print("len of coords", len(coords_transf))
                print("Len of coords found --> %d"%len(coords_transf))
                for idx,objs in enumerate(coords_transf):
                    new_detections = []
                    if len(objs)>0:
                        for obj in objs:
                            roi_id = 0
                            if self.roi is not None:
                                roi_id = self.roi['id']
                            new_detection = Detection(label     = obj[0],  confidence = obj[1], 
                                    center_x  = obj[2][0], center_y = obj[2][1], 
                                    width     = obj[2][2],   height = obj[2][3], 
                                    timestamp = list_frames[idx].timestamp,
                                    id_frame  = list_frames[idx].id,
                                    id_video  = list_frames[idx].id_video,
                                    roi_id    = roi_id)
                            new_detections.append(new_detection)
                        print("DETECTOR:NEW LIST DETECTION RESULT --> Len of detections targeted [%s]"%str(len(new_detections)))
                        self.send((new_detections, list_frames[idx]), self.subscribers_detections_and_frames)
                        self.send(new_detections) # notifies the detections for a single frame.
