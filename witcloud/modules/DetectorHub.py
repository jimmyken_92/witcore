import sys
import os
cpath = os.path.dirname(os.path.abspath(__file__))
print(cpath)

from witcloud.modules import *
from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.modules.CameraReader import CameraReader

from witcloud.models.Frame import Frame
from witcloud.models.Detection import Detection

from witcloud.util import util

import threading

import mysql.connector
from dateutil import tz
from datetime import datetime
import base64
import json
import requests
import uuid
import multiprocessing
import time
import numpy as np
import cv2
import queue

#--------- LOGGING PARAMS -----------------#
import logging
from witcloud.modules import LOGGER_LEVEL
LOGGER_FORMAT = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMAT)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
# ---------- LOGGING PARAMS ---------------- #

class DetectorHub(WitcloudComponent, threading.Thread):
    '''Usage as a thread: calling the start() method after instantiation
        Params:
            `camera_dict`: dict with all params needed.

        
    '''
    def __init__(self, camera_dict):
        # roi={}, label='person', mosaic_size=9, api_detect_server="http://35.205.222.117:8080/image", detection_confidence=0.5, roi_list=None):
        WitcloudComponent.__init__(self, witcloudComponentName='detector'+str(time.time()))
        threading.Thread.__init__(self, name='detector'+str(time.time()))
        self.list_mosaic = []
        self.camera = camera_dict
        self.labels = self.camera.labels
        self.mosaic_size = self.camera.mosaic_size
        self.detection_confidence = self.camera.detection_confidence
        self.api_detect_server = self.camera.gpu_url
        self.roi_list = self.camera.roi_list
        # to use for manage logging and event
        self.visits_metrics = {}
        # to know when was the last event/log
        self.last_log_event = time.time()
        for roi in self.roi_list:
            zone = str(roi['name'])            
            # roi['labels'] = {}
            for label in self.labels:
                self.visits_metrics[label] = {}
                self.visits_metrics[label] = {
                                    'times': [],
                                    'counter': {},
                                    'roi': roi,
                                    'image_idx': None
                                }

            # buscar TODOS los alertas asociadas a esta zona.
            roi_alerts = [alert for alert in self.camera.alerts if alert['zone']==zone]
            roi['events'] = {}
            roi['channels'] = []
            for alert in roi_alerts:
                event = str(alert['event'])
                channel = str(alert['channel'])
                address = str(alert['address'])

                if channel not in roi['channels']:
                    roi['channels'].append(channel)

                if event not in roi['events'].keys():
                    roi['events'][event] = {}

                    if channel not in roi['events'][event].keys():
                        roi['events'][event][channel] = []

                roi['events'][event][channel].append(address)

        # manage alerts with any zone
        self.any_zone_alerts = [alert for alert in self.camera.alerts if alert['zone']=="*"]

        self.buffer = multiprocessing.Queue(maxsize=200)
        self.flag = True
        self.subscribers_detections_and_frames = []
    
        # about datetime
        self.timezone = tz.gettz('SouthAmerica/Asunción')
        self.date_format = '%Y-%m-%d-%H-%M-%S'

        # ABOUT DATABASE CONNECTION =========================
        self.database = self.camera.WARDEN_DATABASE
        self.host_db = self.camera.WARDEN_HOST
        self.user_db = self.camera.WARDEN_USER
        self.password_db = self.camera.WARDEN_PASSWORD
        self.cnx = mysql.connector.connect(user=self.user_db,
                            password=self.password_db,
                            host=self.host_db,
                            database=self.database)
        self.cursor = self.cnx.cursor()

    def register_detections_and_frames(self, subscriber):
        self.register(subscriber, self.subscribers_detections_and_frames)

    def inside_polygon(self, x, y, points):
        """
        Return True if a coordinate (x, y) is inside a polygon defined by
        a list of verticies [(x1, y1), (x2, x2), ... , (xN, yN)].

        Reference: http://www.ariel.com.au/a/python-point-int-poly.html
        """
        n = len(points)
        inside = False
        p1x, p1y = points[0]
        for i in range(1, n + 1):
            p2x, p2y = points[i % n]
            if y > min(p1y, p2y):
                if y <= max(p1y, p2y):
                    if x <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside
        
    def update(self, frame):
        try:
            self.buffer.put(frame)
        except Exception as e:
            logger.error(e)
            self.buffer.get() #lost a frame
            self.buffer.put(frame)

    def consume(self, frame):
        logger.info("consuming a Frame....")
        logger.info("current len of list_mosaic : %d " % len(self.list_mosaic))
        if len(self.list_mosaic) == self.mosaic_size:
            logger.info("Pushing mosaic into DetectorHub Server")
            self.detect(self.list_mosaic)
            self.looking_for_visitors_events(self.list_mosaic)
            self.list_mosaic = []
        else:
            logger.info("feeding mosaic")
            self.list_mosaic.append(frame)

    def _send_gpu_request(self, url, body,files):
        try:
            data = requests.post(self.api_detect_server, body,files=files)
            return json.loads(data.content.decode('utf-8'))
        except Exception as e:
            self.errors.append("PROXY:ERROR Error sending to gpu ---> %s"%str(e))
        # print("data.content -> ", data.content)
        return None

    def _detect(self,mosaic, thresh=.35, hier_thresh=.5, nms=.45):
        filename = util.filename_generator(TMP_FOLDER="/tmp").__next__()
        util.save_image(mosaic, filename)
        # making request to GPU yolo
        files = {
          "image" : open(filename,'rb')
        }
        body = {
            "name_image": uuid.uuid4().hex, 
            "thresh": thresh,
            "hier_thresh": hier_thresh, 
            "nms": nms, 
        }
        logger.info("Sending request to DetectorHub Server -> %s"%self.api_detect_server)
        # data = requests.post(self.api_detect_server, body)
        data = self._send_gpu_request(self.api_detect_server, body,files=files)
        # print("type content", type(content))
        try:
            util.delete_file(filename)
        except Exception as e:
            self.errors.append("DETECTOR:ERROR while deleting temporal mosaic image -->%s"%str(e))
            logger.error("Error while deleting temporal mosaic image -->%s"%str(e))
        
        if data is not None:
            # print("data received -> ", data[0])
            logger.info("data received from DetectorHub server -> %s "%str(data))
        else:
            self.errors.append("DETECTOR:ERROR no data received from DetectorHub server ")
            logger.info("no data received from DetectorHub server ")
        return data

    def detect(self, list_frames):
        ''' Notify detections '''
        assert len(list_frames)>0, 'It seems the list has no elements. Please verify the mosaic_size is greater than zero'
        # create a mosaic
        mosaic = util.make_image_mosaic([frame.data for frame in list_frames])
        data = self._detect(mosaic,thresh=self.detection_confidence)
        #                   DATA FOR DEBUGING PURPOSES
        # data = [['person', 0.9966816902160645, [1174.9049072265625, 274.2868347167969, 100.5470199584961, 217.04067993164062]], ['person', 0.9936493039131165, [1006.1395874023438, 267.54217529296875, 104.13899230957031, 211.79542541503906]], ['person', 0.9869756102561951, [278.5375671386719, 880.8920288085938, 95.56672668457031, 155.1246337890625]], ['person', 0.9427170157432556, [207.8935089111328, 274.2724304199219, 72.28369903564453, 223.2180633544922]], ['person', 0.9184480905532837, [105.19451141357422, 195.85910034179688, 77.68487548828125, 170.68759155273438]], ['person', 0.9178250432014465, [1017.0181884765625, 731.2520141601562, 94.10305786132812, 205.4556427001953]], ['person', 0.8324248194694519, [566.786865234375, 635.6868286132812, 76.12576293945312, 161.60134887695312]], ['person', 0.6995110511779785, [636.1365356445312, 485.6655578613281, 1090.724609375, 760.8880615234375]], ['person', 0.6830428242683411, [1242.5146484375, 635.2398071289062, 50.50928497314453, 135.26541137695312]], ['person', 0.627581775188446, [695.0372924804688, 131.52774047851562, 40.85148620605469, 113.97200775146484]], ['person', 0.5768758058547974, [53.98105239868164, 96.57398223876953, 34.23064422607422, 89.83855438232422]], ['person', 0.5287930369377136, [355.7674560546875, 735.9082641601562, 76.48114776611328, 189.6006622314453]]] 
        if data is None:
            logger.info("No detections ---> %s " % str(data))
            print("No detections ---> %s " % str(data))
            return
        else:
            logger.info("YOLO RESPONSE: {} ".format(data))
        # filter label
        data_filtered = [d for d in data if d[0] in self.labels]
        # now we go back from mosaic
        coords_transf = util.transform_coords(lista=data_filtered,
                                            num_images=self.mosaic_size,
                                            w=list_frames[0].data.shape[1],
                                            h=list_frames[0].data.shape[0])
        new_detections = []

        for roi in self.roi_list:
            roi_id = roi["id"]
            cnts = roi["coordinates"]

            for idx,objs in enumerate(coords_transf):
                if len(objs)==0:
                    # print("No detections ---> %s " % str(objs))
                    continue

                for obj in objs:
                    label_detected = str(obj[0])
                    center_x  = int(obj[2][0])
                    center_y  = int(obj[2][1])
                    width     = int(obj[2][2])
                    height    = int(obj[2][3])
                    
                    if label_detected not in self.labels:
                        continue

                    # print(roi["name"], "qty objs detected in this roi -->", len(objs))
                    
                    # check if detection belongs to this roi
                    try:
                        inside_roi = self.inside_polygon(x=center_x, y=center_y, points=cnts)
                        print("Is detection inside of roi? --", inside_roi)
                    except Exception as e:
                        print("error while compute inside roi ---", str(e))
                        raise(e)
                    # print("this detection is inside this roi --->", inside_roi)
                    if not inside_roi:
                        continue
                    print("person detected in roi_id %s " % str(roi_id))

                    #                   for debuging detections visually                           #
                    cv2.rectangle(list_frames[idx].data,
                                (center_x-int(width//2), center_y-int(height//2)),
                                (center_x+int(width//2), center_y+int(height//2)),
                                (255, 255, 25), thickness=2)
                    if "debug" in self.camera.keys():
                        if self.camera.debug:
                            cv2.imwrite("visitor_detection_debug_%d.png" % int(time.time()), list_frames[idx].data)

                    try:
                        new_detection = Detection(label     = obj[0],  confidence = obj[1], 
                                center_x  = center_x, center_y = center_y, 
                                width     = width,   height = height, 
                                timestamp = list_frames[idx].timestamp,
                                id_frame  = list_frames[idx].id,
                                id_video  = list_frames[idx].id_video,
                                roi_id    = roi_id)
                        new_detections.append(new_detection)
                    except Exception as e:
                        print("Error ........... ", str(e))
                        raise(e)
                    # print("*******************************************************************************************")
                    # print("LABEL IN USE --", label_detected)

                    # print("keys in roi --->", roi['labels'].keys())

                    # using timestamp as a key to store the times a label appears
                    # ts is truncate to seconds and casted to string
                    ts = int(list_frames[idx].timestamp//1000)
                    try:
                        self.visits_metrics[label_detected]['times'].append(ts)
                    except Exception as e:
                        print("errror while adding new ts -->", str(e))

                    if ts not in self.visits_metrics[label_detected]['counter'].keys():
                        # print("only the first time. counter is zero --", label_detected, "at this time -->", ts)
                        self.visits_metrics[label_detected]['counter'][ts] = 0
                    else:
                        # if exists, increment ocurrence of this label
                        try:
                            print("incrementing.....")
                            self.visits_metrics[label_detected]['counter'][ts] += 1
                        except Exception as e:
                            print("errror while incrementing -->", str(e))
                        try:
                            # ADD IMAGE
                            self.visits_metrics[label_detected]['image_idx'] = idx
                        except Exception as e:
                            print("errror while adding image -->", str(e))

        if len(new_detections):
            # logger.info("DETECTOR:NEW LIST DETECTION RESULT --> In roi [%s] Len of detections targeted [%s]" % (roi_id, str(len(new_detections))))
            # self.send((new_detections, list_frames[idx]), self.subscribers_detections_and_frames)
            self.send(new_detections) # notifies the detections for a single frame.

    def looking_for_visitors_events(self, list_frames):
        # check how lon ago was the last event/log
        if time.time() - self.last_log_event < 60:
            return
        self.last_log_event = time.time()
        # check for alerts and logging
        if len(list_frames) < 1:
            return
        print("in looking_for_visitors_events FUNC")
        for label in self.labels:
            # print(roi)
            max_visits = self.visits_metrics[label]["roi"]["app_settings"]["max_occupation"]
            min_visits = self.visits_metrics[label]["roi"]["app_settings"]["min_occupation"]

            # for label in labels_in_roi:
            ts_now = int(time.time())
            print("ts_now --", ts_now)

            # this label has not detections yet
            if not len(self.visits_metrics[label]['times']):
                print("this label has not detections yet  ---> ", label)
                continue

            last_ts = self.visits_metrics[label]['times'][-1]
            print("last_ts -->", last_ts)
            #to compute how old is the data
            # if (ts_now - last_ts) < 60: # is not older than a minute
            image_idx = self.visits_metrics[label]['image_idx']
            if image_idx is None:
                continue
            counter = self.visits_metrics[label]['counter'][last_ts]

            # NOTE: here the counter needs to be normalized by the camera framerate 
            counter = int(round(counter/self.camera.fps_grab_img))
            print("label: [%s] counter: [%d] " % (label, counter))
            
            image = list_frames[image_idx].data
            # check if there is alerts for this label - and check if thershold is 
            if counter > max_visits:
                # CROWDED EVENT ARISES
                assert image is not None, 'image is None'
                image_url = self.upload_image(image, last_ts )
                self.cursor.execute("INSERT INTO log_log(url,registration_date,camera_id,event,zone,type_fuctionality) VALUES (%s,%s,%s,%s,%s,%s)",(str(image_url), datetime.now(tz=self.timezone), int(self.camera.cam_id), CROWDED_VISITS_EVENT, self.visits_metrics[label]["roi"]["name"], VISITORS_APP_NAME))
                self.cnx.commit()

                self.send_visitor_notification(zone_name=self.visits_metrics[label]["roi"]["name"],
                                                event=CROWDED_VISITS_EVENT,
                                                image_url=image_url)

            elif counter < min_visits:
                # EMPTY EVENT ARISES
                assert image is not None, 'image is None'
                image_url = self.upload_image(image, last_ts)
                self.cursor.execute("INSERT INTO log_log(url,registration_date,camera_id,event,zone,type_fuctionality) VALUES (%s,%s,%s,%s,%s,%s)",(str(image_url), datetime.now(tz=self.timezone), int(self.camera.cam_id), EMPTY_VISITS_EVENT, self.visits_metrics[label]["roi"]["name"], VISITORS_APP_NAME))
                self.cnx.commit()

                self.send_visitor_notification(zone_name=self.visits_metrics[label]["roi"]["name"],
                                                event=EMPTY_VISITS_EVENT,
                                                image_url=image_url)
            else:
                print("qty of %s is within a normal value [%d] " % ( label, counter ))
            
            # else:
            #     # this label does not appear since a minute ago
            #     # so its counter is zero
            #     print("here")

    def upload_image(self, dataimage, ts):
        mydatetime = datetime.fromtimestamp(ts, self.timezone)
        str_date = mydatetime.strftime("%Y_%m_%d_%H_%M_%S_")
        print("str_date -->", str_date)
        fullname = str_date + util.code_generator(size=20) + '.png'
        print("uploading image about some visitor event")
        image_url = util.upload_image_to_gcs_bucket(image_data=dataimage,
                                        image_name=fullname,
                                        bucket_name="witcloudvisitors")
        logger.info("image url ==> %s " % str(image_url))
        return image_url

    def send_visitor_notification(self, zone_name, event, image_url):
        """
            Method to be used only for visitors app notifications.
            TODO: Create a new WitcloudComponent to manage centraly all about app notifications/alerts.
        """
        print(" THERE IS A CHANNEL FOR THIS ???? ")
        print("event --->", event)
        print("zone_name --->", zone_name)
        # find roi of zone_name
        roi = [roi_ for roi_ in self.roi_list if roi_["name"]==zone_name][0]
        # send notification  ================================================ START
        events_in_roi = roi['events'].keys()
        body_event = {}
        body_event['notification_server'] = self.camera.notification_server + '/oculus_notification/'
        body_event['zone_name'] = roi['name']
        body_event['video_url'] = image_url
        body_event['timestamp'] = int(time.time()*1000)
        
        for event_in_roi in events_in_roi:
            print("event_in_roi -->", event_in_roi)
            event_in_roi = str(event_in_roi)
            if event_in_roi == event:
                channels_in_event = roi['events'][event].keys()
                print("==event ", event)
                print("==channels_in_event ", channels_in_event)
                for channel in channels_in_event:
                    address_list = roi['events'][event][channel]
                    body_event['channel'] = channel
                    body_event['address'] = json.dumps(address_list)
                    
                    logger.info("NOTIFYING EVENT")
                    try:
                        self.update_visitor_notification(body_event)
                    except Exception as e:
                        print("ERROR WHILE TRYING TO NOTIFY ---->", str(e))

        for any_alert in self.any_zone_alerts:
            if any_alert["event"] == event:
                channel = any_alert["channel"]
                address = any_alert["address"]
                body_event['channel'] = channel
                body_event['address'] = json.dumps([address])

                logger.info("NOTIFYING EVENT")
                try:
                    self.update_visitor_notification(body_event)
                except Exception as e:
                    print("ERROR WHILE TRYING TO NOTIFY ---->", str(e))

        # send notification  ================================================ END

    def update_visitor_notification(self, body):
        print("SENDING A VISITORS APP NOTIFICATION")
        logger.info("SENDING A VISITORS APP NOTIFICATION")
        assert 'timestamp' in body.keys(), 'Body does not contain an timestamp field. Please, provides it'
        assert 'zone_name' in body.keys(), 'Body does not contain an zona_name field. Please, provides it'
        assert 'channel' in body.keys(), 'Body does not contain an channel field. Please, provides it'
        assert 'address' in body.keys(), 'Body does not contain an address field. Please, provides it'
        assert 'video_url' in body.keys(), 'Body does not contain an url field. Please, provides it'
        notification_server = body['notification_server']
        try:
            logger.info(body)
            r = requests.post(notification_server,data=body)
        except Exception as e:
            logger.error(" Error while sending notification %s " % str(e))
            raise
        print("RESULT OF SENDING NOTIFICATOIN:", r.text)
        logger.info("Notification sent -- %s " % r.text)

    def run(self):
        logger.info("Started")
        while self.flag:
            try:
                newFrame = self.buffer.get() #waits for a new element
            except queue.Empty:
                pass
            try:
                self.consume(newFrame)
            except Exception as e:
                logger.error("ERROR while getting new frame from buffer %s " % str(e))
                self.errors.append("noFramesInBuffer")
                time.sleep(0.1)
                raise(e)
            time.sleep(0.001)
