from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
from witcloud.models.Frame import Frame

import time
import cv2
import face_recognition
from hashlib import sha256

class FaceComparator(WitcloudComponent):
	def __init__(self):
		WitcloudComponent.__init__(self)
		self.known = {}
		print("### FaceComparator Started ###")

	def randomID(self, encoding):
		create_id = sha256(str(encoding).encode()).hexdigest()
		return str(create_id[:5])

	def comparator(self, data):
		if data is not None:
			boxes, encodings, newFrame = data
			print("INFO:FaceComparator: Compare faces")
			frame = newFrame.data
			lst_detections = []
			names = []

			if not self.known:
				for encoding in encodings:
					self.known = {
						"encodings": [encoding],
						"names": [self.randomID(encoding)]
					}
					names.append("unknown")

			for encoding in encodings:
				if self.known:
					matches = face_recognition.compare_faces(self.known["encodings"], encoding)
					name = "unknown"

					if True in matches:
						matchesid = [i for (i, b) in enumerate(matches) if b]
						counts = {}

						for i in matchesid:
							name = self.known["names"][i]
							counts[name] = counts.get(name, 0) + 1
						name = max(counts, key=counts.get)

					else:
						self.known["encodings"].append(encoding)
						self.known["names"].append(self.randomID(encoding))

					# if name != "unknown":
					names.append(name)

			# if len(names) > 0:
			for ((x1, x2, y1, y2), name) in zip(boxes, names):
				detections_data = Detection({
					"label": str(name),
					"confidence": 0,
					"x1": int(x1),
					"x2": int(x2),
					"y1": int(y1),
					"y2": int(y2),
					"timestamp": str(time.time()),
					"id_frame": 0,
					"id_video": 0,
					"roi_id": 0
				})
				lst_detections.append(detections_data)
			self.send((lst_detections, newFrame))

	def update(self, data):
		self.comparator(data)
