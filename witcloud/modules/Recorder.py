import sys
import os
import time
import json
cpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(cpath, '../../'))

# global variables from __init__.py file
from witcloud.modules import *

#utils
from witcloud.util import util

#libs
import subprocess as sp
from datetime import datetime
from dateutil import tz

#--------- LOGGING PARAMS -----------------#
import logging
from witcloud.modules import LOGGER_LEVEL
LOGGER_FORMAT = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMAT)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)

class Recorder(object):
    '''
        Start a Recorder instance.
    '''

    def __init__(self, camera):

        self.camera = camera
        self.flag = True
        #about datetime and timezone
        self.timezone = tz.gettz(self.camera.timezone)

    def start_recorder(self):
        ts = datetime.now(tz=self.timezone).ctime().replace(' ', '_')
        ts = ts.replace('__', '_')
        print("ts --->", ts)
        self.filename = "/tmp/video_%s_%s_%s_%%d.mp4" % (str(self.camera.center), str(self.camera.cam_id), ts)
                    # '-codec', 'copy',
        command = ['ffmpeg',
                    '-i', self.camera.url,
                    '-vcodec', 'mpeg4',
                    '-map', '0',
                    '-r', str(self.camera.fps_grab_img),
                    '-f', 'segment',
                    '-segment_time', str(self.camera.duration),
                    '-segment_format', 'mp4',
                    '-filter:v',
                    '"setpts=%s*PTS"' % self.camera.fps_grab_img,
                    '-reset_timestamps', '1',
                    self.filename]
        cmd_str = " ".join(command)
        print("=============================================================")
        print(' '.join(command))
        print("=============================================================")
        logger.error("Error while starting FFMPEG command [%s] " % ' '.join(command))
        try:
            logger.info("Starting recorder with FFMPEG program")
            logger.info(" : %s " % self.filename)
            result = os.system(command=cmd_str)
            # result = str(sp.check_output(command))
            logger.info("Output command execution [%s] " % result)
            return True
        except Exception as e:
            logger.error("Error while starting FFMPEG command [%s] " % ' '.join(command))
            logger.error(str(e))
            return False

    def run(self):
        started = False
        while self.flag:
            if not util.check_in_schedule(self.camera):
                logger.info("Out of schedule. Going down.")
                # wait 1 minute
                time.sleep(60)

            else:
                if not started:
                    resp = self.start_recorder()
                if resp:
                    started = True
                    logger.info("Recording!")
                time.sleep(10)
        logger.info("End the main thread!")

if __name__ == '__main__':
    # auth environment variable
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    CREDENTIAL_PATH = ROOT_FOLDER + "/../../credentials/witcloud_credentials.json"
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = CREDENTIAL_PATH
    print("CREDENTIAL PATH USED --> ", CREDENTIAL_PATH)
    print("SUCCESSFULLY EXPORTED GAC CREDENTIALS")

    #preparing camera object
    camera_json = sys.argv[1]
    camera = json.load(open(camera_json))
    logger.info(camera)
    while True:
        if util.check_in_schedule(camera):
            myRecorderApp = Recorder(camera)
            myRecorderApp.run()
            break
        else:
            logger.info("NOT IN SCHEDULE YET")
            time.sleep(60)
