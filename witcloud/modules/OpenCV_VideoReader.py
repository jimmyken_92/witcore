from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
import threading
from datetime import datetime, timedelta

#--------- LOGGING PARAMS -----------------#
import logging
from witcloud.modules import LOGGER_LEVEL
LOGGER_FORMAT = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMAT)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
#---------- LOGGING PARAMS ----------------#

#---------- VIDEO-READER PARAMS ----------#
import cv2
import time
#---------- VIDEO-READER PARAMS ----------#

class OpenCV_VideoReader(WitcloudComponent, threading.Thread):
	def __init__(self, filename, frame_rate, doPyrDown=False, video_date=None, WitcloudComponentName="VideoReader"):
		WitcloudComponent.__init__(self, witcloudComponentName=WitcloudComponent)
		threading.Thread.__init__(self)

		self.filename = filename
		self.doPyrDown = doPyrDown
		self.init_datetime = video_date if video_date else datetime.utcnow()
		self.frame = None
		self.frame_id = 1
		self.id_frame = 0
		self.id_video = 0
		self.frame_rate = frame_rate

		assert frame_rate > 0

	def new_frame(self, frame):
		try:
			frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
			gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

			if self.doPyrDown:
				frame = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)

			self.init_datetime += timedelta(microseconds=self.frame_id * self.frame_rate / 1E6)
			self.frame = Frame(data=frame, data_gray=gray_frame, timestamp=time.mktime(self.init_datetime.timetuple()) + self.init_datetime.microsecond / 1E6, id=self.frame_id)
			self.id_frame += 1

			return self.frame
		except:
			return None

	def run(self):
		""" Run the thread """
		print("INFO:VideoReader: Started thread")

		if self.filename == "Camera":
			file = 0
		elif self.filename == "Integrated":
			file = 1
		else:
			file = self.filename

		capture = cv2.VideoCapture(file)
		# capture.set(cv2.CAP_PROP_CONVERT_RGB, True)
		capture.set(cv2.CAP_PROP_FPS, 5.0)

		while True:
			frame = capture.read()[1]
			newFrame = self.new_frame(frame)
			if not newFrame:
				return None

			newFrame.id = self.id_frame
			newFrame.id_video = self.id_video

			self.send(newFrame)

		print("INFO:VideoReader: End thread")
