from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
import numpy as np
from witcloud.util.FFmpegWriter import FFmpegWriter
import skvideo.io
from witcloud.util import util
import threading
import asyncio
import multiprocessing
import time
import os

#--------- LOGGING PARAMS -----------------#
import logging
from witcloud.modules import LOGGER_LEVEL
LOGGER_FORMAT = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMAT)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
# ---------- LOGGING PARAMS ---------------- #

class StreamingWriter(WitcloudComponent, threading.Thread):
    def __init__(self, camera, inputdict=None, outputdict=None):
        WitcloudComponent.__init__(self, witcloudComponentName=camera.cam_id)
        threading.Thread.__init__(self,  name=camera.cam_id)
        self.camera = camera
        logger.info("Streming Writer Started")
        print("Streming Writer", camera)
        self.system_id = int(camera.system_id)
        self.cam_id = int(camera.cam_id)
        self.center_id = int(camera.center_id)
        self.node_id = int(camera.node_id)
        self.filename = '/tmp/%s_streaming.mp4'%self.cam_id
        self.inputdict = inputdict
        self.outputdict = outputdict
        self.flag = True
        self.writter = None
        self.buffer = multiprocessing.Queue(maxsize=200)
        self.videoWriter = FFmpegWriter(filename=self.filename)

    def update(self, frame:Frame):
        #TODO limit size of buffer/override old elements
        try:
            if self.start_time is None:
                self.start_time = frame.timestamp
            self.end_time = frame.timestamp
            if self.width is None and self.height is None:
                self.height = frame.height
                self.width = frame.width
            else:
                assert frame.data.shape == ( self.width, self.height, 3), "All frame shapes should be equal to %s, provided %s"%(str((self.height, self.width, 3)), str(frame.data.shape))
            #logger.info("adding new frame to buffer in videowriter")
            self.buffer.put(frame)
        except Exception as e:
            logger.exception(e)
            self.buffer.get() #lost a frame
            self.buffer.put(frame)

    def consume(self, frame:Frame):
        logger.info("Writing A frame INTO stremaing file....")
        self.writter.writeFrame(frame.data)

    def run(self):
        logger.info("Started")
        while True:
            if self.flag:
                try:
                    newFrame = self.buffer.get() #waits for a new element
                    self.consume(newFrame)
                except Exception as e:
                    logger.info(e)
                    time.sleep(0.1)
            else:
                logger.info("wait for a new videowriter object..... [:8) %s "%str(time.time()))
                pass
