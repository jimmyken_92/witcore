''' README: 
    Every constant must have only one blank space from 
    the variable name, the equal sign and the value.
    Example:
        flag_to_feature_n = value_of_that_feature
    Some incorrect forms would be as follows:
        flag_to_feature_n =value_of_that_feature
        flag_to_feature_n= value_of_that_feature
        flag_to_feature_n=  value_of_that_feature
        (etc.)
'''
# seconds to send metrics to endpoint
FREQUENCY_TO_SAVE_METRICS = 60
LAST_OBSERVATIONS = 50
# max time to send status if no error
TIME_TO_SEND_STATUS = 7200
# to save detections in node manager as a json file
ENABLE_SAVE_DETECTIONS_LOCAL = False
FREQUENCY_TO_SAVE_DETECTIONS_LOCAL = 60 # in seconds
# experimental features 
ENABLE_CLOUD = False
ENABLE_SAVE_DETECTIONS = True
ENABLE_SAVE_EMOTION_DETECTIONS = False
ENABLE_SAVE_AGE_GENDER_DETECTIONS = False
ENABLE_SAVE_BLOB_COUNTER = False
ENABLE_SAVE_GROUPED_RTM = False
ENABLE_SAVE_SHELF_METRICS = False
ENABLE_HEAT_MAPS = False
ENABLE_VIDEO_WRITER_LABELLED = False
ENABLE_DETECTION_DRAWER = False
ENABLE_PUB_SUB = False
# for pub/sub comms in oculus project
OCULUS_SERVER_TOPIC = 'oculus_server_topic'
SECONDS_BETWEEN_NOTIFICATION = 60
BACKGROUND_MOTION_DEBUG = False
MAX_FRAMES_CONSECUTIVE_WITH_MOTION = 200 # if FPS=10.0 --> the max time with motion events until send a notification will be 1.0 second
resize_factor = 1.0
FREQ_TO_CHECK_SHELF_LOG_AND_NOTIF = 30
FREQ_TO_CHECK_SECURITY_LOG_AND_NOTIF = 2
DEBUG_SHELF_APP = False
DEBUG_WRITE_IMG = False
MAX_HISTORY = 500
THRESH_EMPTY_SHELF = 15
THRESH_WARNING_SHELF = 55
LEVEL_EMPTY = 2
LEVEL_WARNING = 3
LEVEL_EMPTY_EVENT = str("empty_shelf")
LEVEL_WARNING_EVENT = str("warning_shelf")
CAMERA_ERROR_EVENT = str("camera_error")
LEVEL_OK = 1
VISITORS_APP_NAME = "Visitors"
SHELVES_APP_NAME = "Shelves"
RECOGNITION_APP_NAME = "Recognition"
SHELFMETRIC_TABLE = "shelfmetrics_shelfmetrics"
CAMERA_THREAD_RETRIES = 10
EMPTY_VISITS_EVENT = str("empty_zone")
CROWDED_VISITS_EVENT = str("crowded_zone")
MAXIMUM_WAITING_EVENT = str("maximum_waiting")
FACE_DETECTED_EVENT = str("face_detected")

import logging
LOGGER_LEVEL = logging.DEBUG
LOGGER_FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

