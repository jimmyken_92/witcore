from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
from witcloud.models.Frame import Frame
from witcloud.util import haarcascade

import cv2
import time

# label = "BODY", "FACE" or "EYES"

class HaarDetector(WitcloudComponent):
	def __init__(self, label):
		WitcloudComponent.__init__(self)

		self.label = label
		# set the cascade classifier
		self.cascade = haarcascade.haarcascade(self.label)
		self.detector = cv2.CascadeClassifier(self.cascade)

	def haarDetect(self, frame):
		if frame is not None:
			print("INFO:Detected " + self.label)
			
			lst_detections = []
			# detect the LABEL in the frame
			faces = self.detector.detectMultiScale(
				cv2.cvtColor(frame.data, cv2.COLOR_BGR2GRAY), scaleFactor=1.1, minNeighbors=5,
				minSize=(30,30), flags=cv2.CASCADE_SCALE_IMAGE)

			for x1, x2, y1, y2 in faces:
				detections_data = Detection({
					"label": 0,
					"confidence": 0,
					"x1": int(x1),
					"x2": int(x2),
					"y1": int(y1),
					"y2": int(y2),
					"timestamp": str(time.time()),
					"id_frame": 0,
					"id_video": 0,
					"roi_id": 0
				})
				lst_detections.append(detections_data)
			self.send((lst_detections, frame))

	def update(self, frame:Frame):
		self.haarDetect(frame)
