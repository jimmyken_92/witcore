from witcloud.modules.Detector import Detector
import detection.yolo_cpu as yolo
import os
from witcloud.util import util

class DetectorLocal(Detector):
    def __init__(self, label='person', mosaic_size=9):
        Detector.__init__(self, label='person', mosaic_size=9 )
        self.net = yolo.load_witcloud_net()
        self.meta = yolo.load_witcloud_meta()
    
    def _detect(self, mosaic, thresh=.35, hier_thresh=.5, nms=.45):
        filename = util.filename_generator(TMP_FOLDER="/tmp").__next__()
        util.save_image(mosaic, filename)
        values = yolo.detect(self.net, self.meta, filename)
        util.delete_file(filename)
        return values
