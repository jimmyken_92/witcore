from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
from witcloud.util import models

import numpy as np
import cv2
import time

class AgeGenderDetector(WitcloudComponent):
    def __init__(self):
        WitcloudComponent.__init__(self)

    def detector(self, data):
        if data is not None:
            faces, newFrame = data
            print("INFO:AgeGenderDetector: Reading frame...")
            frame = newFrame.data
            frame_id = newFrame.id
            lst_detections = []
            detections_data = []

            # parameters
            MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746) # default mean values
            age_list = [
                "(0, 2)",
                "(4, 6)",
                "(8, 12)",
                "(15, 20)",
                "(25, 32)",
                "(38, 43)",
                "(48, 53)",
                "(60, 100)"
            ]
            gender_list = ["Male", "Female"]

            age_net = cv2.dnn.readNetFromCaffe(models.deployAgeProto(), models.ageNetModel())
            gender_net = cv2.dnn.readNetFromCaffe(models.deployGenderProto(), models.genderNetModel())

            # loop over the detections
            for x1, x2, y1, y2 in faces:
                # get face
                face_img = frame[y1:y2, x1:x2].copy()
                blob = cv2.dnn.blobFromImage(face_img, 1, (227, 227), MODEL_MEAN_VALUES, swapRB=False)

                # gender
                gender_net.setInput(blob)
                gender_preds = gender_net.forward()
                gender = gender_list[gender_preds[0].argmax()]

                # age
                age_net.setInput(blob)
                age_preds = age_net.forward()
                age = age_list[age_preds[0].argmax()]

                detections_data = Detection({
                    "label": str("{}, {}".format(gender, age)),
                    "confidence": 0,
                    "x1": int(x1),
                    "x2": int(x2),
                    "y1": int(y1),
                    "y2": int(y2),
                    "timestamp": str(time.time()),
                    "id_frame": frame_id,
                    "id_video": 0,
                    "roi_id": 0
                })
                lst_detections.append(detections_data)
            self.send((lst_detections, newFrame))

    def update(self, data):
        self.detector(data)
