from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
from witcloud.util import models

# ---------- EMOTION-DETECTOR PARAMS ---------------- #
import cv2
import time
import numpy as np
from keras.models import load_model
from witcloud.util.mood.preprocessor import preprocess_input
# ---------- EMOTION-DETECTOR PARAMS ---------------- #

class EmotionDetector(WitcloudComponent):
    """
        FER: Face Emotion Recognition using Tensorflow and hdf5 models from Kaggle competition.
        Source: https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge
    """

    # (*) fer2013 emotion classification test accuracy: 66%

    def __init__(self):
        WitcloudComponent.__init__(self)

        self.emotion_model_path = models.emotionModel()
        self.emotion_labels = {0:'angry', 1:'disgust', 2:'fear', 3:'happy', 4:'sad', 5:'surprise', 6:'neutral'}

    def emotionDetector(self, data):
        if data is not None:
            detected_faces, newFrame = data
            print("INFO:EmotionDetector: Reading frame...")
            frame = newFrame.data
            lst_detections = []
            emotion_window = []

            gray_image = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            emotion_classifier = load_model(self.emotion_model_path)
            emotion_target_size = emotion_classifier.input_shape[1:3]

            for x1, x2, y1, y2 in detected_faces:
                gray_face = gray_image[y1:y2, x1:x2]
                try:
                    gray_face = cv2.resize(gray_face, (emotion_target_size))
                except:
                    continue

                gray_face = preprocess_input(gray_face, True)
                gray_face = np.expand_dims(gray_face, 0)
                gray_face = np.expand_dims(gray_face, -1)
                # print("###################################################", gray_face.shape)

                emotion_prediction = emotion_classifier.predict(gray_face)
                emotion_probability = np.max(emotion_prediction)
                emotion_label_arg = np.argmax(emotion_prediction)
                emotion_text = self.emotion_labels[emotion_label_arg]
                emotion_window.append(emotion_text)
                # print(emotion_text)

                detections_data = Detection({
                    "label": emotion_text,
                    "confidence": 0,
                    "x1": int(x1),
                    "x2": int(x2),
                    "y1": int(y1),
                    "y2": int(y2),
                    "timestamp": str(time.time()),
                    "id_frame": 0,
                    "id_video": 0,
                    "roi_id": 0
                })
                lst_detections.append(detections_data)
            self.send((lst_detections, newFrame))

    def update(self, data):
        self.emotionDetector(data)
