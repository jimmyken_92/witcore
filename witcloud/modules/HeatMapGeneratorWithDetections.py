from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
from witcloud.models.Frame import Frame
from witcloud.models.HeatMap import HeatMap
from witcloud.util.image import compute_heatmap_matrix

import time
import threading

#--------- LOGGING PARAMS -----------------#
from witcloud.modules import LOGGER_LEVEL
from witcloud.modules import LOGGER_FORMATTER
import logging
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMATTER)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
# ---------- LOGGING PARAMS ---------------- #

class HeatMapGeneratorWithDetections(WitcloudComponent, threading.Thread):
    ''' 
        Save Detections in a buffer and eventually returns a HeatMap matrix.
        Only receive a list of `Detection` objects.
        Params:
            - camera : camera dict
            - scale  : scale current frame size used by CameraReader to set heatmap matrix size
            - smooth_flag : bool flag to determine the way heatmap matrix is computated: discrete or smoothed.
            - witcloudComponentName : name of component + identifier
    '''

    def __init__(self, camera, scale=0.1, witcloudComponentName="HeatMapGenWithDetec", smooth_flag=False):
        WitcloudComponent.__init__(self, witcloudComponentName)
        threading.Thread.__init__(self) #, None, None)
        self.flag = True  # flag to control the main thread
        self.detections = []  # buffer of detections
        self.camera = camera
        self.scale = scale
        self.smooth_flag = smooth_flag
        self.heatmap_width = int(self.camera.heatmap_width*self.scale)
        self.heatmap_height = int(self.camera.heatmap_height*self.scale)
        self.heatmap = [[0 for _ in range(100)]
                        for _ in range(self.heatmap_height)]
        self.frame = None

    def update(self, list_detections_frame):
        list_detections, frame = list_detections_frame
        self.frame = frame
        for d in list_detections:
            self.detections.append(d)

    def compute_heatmap_matrix(self):
        total_objects = len(self.detections)
        frame = self.frame
        logger.info("Computing heatmap matrix with points: %s" % total_objects)
        # logger.info("Heatmap size: width: [%d] height: [%d]" % (self.heatmap_width, self.heatmap_height))
        try:
            if total_objects > 0:
                timestamp = self.detections[-1].timestamp
                heatmap = compute_heatmap_matrix(list_detections=self.detections,
                                                 heatmap_width=self.heatmap_width,
                                                 heatmap_height=self.heatmap_height,
                                                 smooth_flag=self.smooth_flag,
                                                 scale=self.scale)
                message = HeatMap(matrix=heatmap,
                                  timestamp=timestamp,
                                  cam_id=self.camera.cam_id,
                                  frame=frame)
                logger.info("A new heatmap published")
                self.send(message)
        except:
            logger.exception("Failed computing heatmap matrix")

    def run(self):
        logger.info("Started")
        self.init_time = time.time()
        while self.flag:
            if time.time() - self.init_time > int(self.camera.duration):
                self.compute_heatmap_matrix()
                self.init_time = time.time()
            time.sleep(0.01)

        logger.info("Stop routines activated")
        super().stop_component()
