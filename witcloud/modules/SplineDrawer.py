from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
from witcloud.models.Spline import Spline
import witcloud.util as util
import colorsys
import numpy as np

class SplineDrawer(WitcloudComponent):
    ''' Receive Splines and Frames, save a buffer of both, 
        and eventually publish drawed Frames
        when a match timestamp is found.'''
    def __init__(self, flag_draw_rect=True, flag_draw_trace=False, isBlueprintDrawer=False, margin=100):
        WitcloudComponent.__init__(self)
        self.frames = []
        self.splines = []
        self.isBlueprintDrawer = isBlueprintDrawer
        self.margin = margin
        self.flag_draw_rect = flag_draw_rect
        self.flag_draw_trace = flag_draw_trace

    def close(self):
        # list_frame_detections = filter(lambda detection: detection.timestamp==frame.timestamp, self.detections)
        print("len of frames ->", len(self.frames))
        print("len of splines ->", len(self.splines))
        for frame in self.frames:
            for k, spline in enumerate(self.splines):
                ''' retrieve the coordinates of a spline for this frame '''
                frame_inertialDetection = []
                trace_to_draw = []
                for ind_, inertialDetection in enumerate(spline.coordinates):
                    if inertialDetection.timestamp==frame.timestamp:
                        frame_inertialDetection.append(inertialDetection)
                        if self.flag_draw_trace:
                            for index_ in range(1, min(15, ind_)):
                                trace_to_draw.append(spline.coordinates[ind_-index_])

                
                for coordinate in frame_inertialDetection:
                    color = colorsys.hsv_to_rgb(0.618033 * k, 1, 1)
                    color=(int(255*color[0]),int(255*color[1]),int(255*color[2]))
                    if self.flag_draw_rect:
                        frame.data = util.image.draw_rectangle(frame.data, coordinate.x, 
                                                    coordinate.y, coordinate.width, coordinate.height, color=color)

                    label="%3.4f"%(frame.timestamp*1000-spline.coordinates[0].timestamp*1000)
                    frame.data = util.image.draw_text(frame.data, spline.id, label=label, color=color, position=(10, 10+20*k))

                    frame.data = util.image.draw_point(image=frame.data, center_x=int(coordinate.y),
                                                       center_y=int(coordinate.x), radius=3, color=color)

                if self.flag_draw_trace:
                    for j, coordinate in enumerate(trace_to_draw):
                        frame.data = util.image.draw_point(image=frame.data, center_x=int(coordinate.y), center_y=int(coordinate.x),
                                                           radius=int(15-10*j/20.0), color=color)
            
            self.frames.remove(frame)
            ''' notify the new frame to subscribers '''
            self.send(frame)

    def update_frame(self, frame:Frame):
        # print("Spline Drawer updating frame...")
        if self.isBlueprintDrawer:
            blueprintFrame = Frame(data=np.zeros((frame.data.shape[0]+self.margin, frame.data.shape[1]+self.margin , 3), dtype=np.uint8),
                                   timestamp=frame.timestamp)
            self.frames.append(blueprintFrame)
        self.frames.append(frame)
        
        
    def update_spline(self, spline:Spline):
        print("Detection Drawer updating spline list...")    
        # print("Detection_drawer", str(detection.__dict__))    
        self.splines.append(spline)
