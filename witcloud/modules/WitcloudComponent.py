import time

class WitcloudComponent(object):
    def __init__(self, witcloudComponentName="WitcloudComponent"):
        self.subscribers = []
        self.components = []
        self.errors = []
        self.flag = False
        if witcloudComponentName:
            self.witcloudComponentName = witcloudComponentName
        else:
            self.witcloudComponentName = type(self).__name__

    def get_status(self):
        if hasattr(self, 'components'):
            for c in self.components:
                if len(c.errors):
                    return 'ERROR'
                else:
                    return 'OK'
        else:
            if len(self.errors):
                return 'ERROR'
            else:
                return 'OK'

    def __str__(self):
        return str(self.witcloudComponentName)

    def unsubscribe(self, subscriber):
        self.subscribers.remove(subscriber)

    def subscribe(self, subscriber, list_subscribers=None):
        if list_subscribers is None:
            self.subscribers.append(subscriber)
        else:
            list_subscribers.append(subscriber)

    def send(self, data, list_subscribers=None):
        if list_subscribers is None:
            list_subscribers = self.subscribers
        for subscriber in list_subscribers:
            if callable(subscriber):
                subscriber(data)
            else:
                subscriber.update(data)

    def add_error(self, error):
        self.errors.append(error)
