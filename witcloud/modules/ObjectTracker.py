from __future__ import division, print_function, absolute_import

from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
import numpy as np
import time

# ---------- TRACKING PARAMS ---------------- #
from PIL import Image
from witcloud.util.yolo import YOLO
from witcloud.util.deep_sort import nn_matching
from witcloud.util.deep_sort.tracker import Tracker
# ---------- TRACKING PARAMS ---------------- #

class ObjectTracker(WitcloudComponent):
    def __init__(self, metric="cosine"):
        WitcloudComponent.__init__(self)

        self.metric = metric
        self.max_cosine_distance = 0.3
        self.nn_budget = None

        metric = nn_matching.NearestNeighborDistanceMetric(self.metric, self.max_cosine_distance, self.nn_budget)
        self.tracker = Tracker(metric)

    def tracking(self, data):
        if data is not None:
            print("INFO:ObjectTracker: Reading frame...")
            detections, newFrame = data
            frame = newFrame.data
            # print(detections)

            self.tracker.predict()
            self.tracker.update(detections)
            lst_detections = []

            for track in self.tracker.tracks:
                bbox = track.to_tlbr()

                detections_data = Detection({
                    "label": track.track_id,
                    "confidence": 0,
                    "x1": int(bbox[0]),
                    "x2": int(bbox[2]),
                    "y1": int(bbox[1]),
                    "y2": int(bbox[3]),
                    "timestamp": str(time.time()),
                    "id_frame": 0,
                    "id_video": 0,
                    "roi_id": 0
                })
                lst_detections.append(detections_data)
            self.send((lst_detections, newFrame))

    def update(self, data):
        self.tracking(data)
