from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection

# ---------- LOGGING PARAMS ---------------- #
import logging
from witcloud.modules import LOGGER_LEVEL
LOGGER_FORMAT = logging.Formatter("[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s","%m-%d %H:%M:%S")
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMAT)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
# ---------- LOGGING PARAMS ---------------- #

class DetectorMock(WitcloudComponent):
    def __init__(self):
        WitcloudComponent.__init__(self, witcloudComponentName="DetectorMock")

    def frameRateCheck(self, data):
        if data is not None:
            detections, newFrame = data
            lst_detections = []

            for x1, x2, y1, y2 in detections:
                detections_data = Detection({
                    "label": 0,
                    "confidence": 0,
                    "x1": int(x1),
                    "x2": int(x2),
                    "y1": int(y1),
                    "y2": int(y2),
                    "timestamp": 0,
                    "id_frame": 0,
                    "id_video": 0,
                    "roi_id": ""
                })
                lst_detections.append(detections_data)
            self.send((lst_detections, newFrame))

    def update(self, data):
        self.frameRateCheck(data)
