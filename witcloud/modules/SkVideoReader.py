import WitcloudComponent
from witcloud.models.Frame import Frame
from witcloud.util import image as image_util

import skvideo.io
from skimage.transform import resize
from datetime import datetime, timedelta
import sys
import os
import time
import threading
import numpy as np

class VideoReader(WitcloudComponent, threading.Thread):
    def __init__(self, filename, frame_rate, video_date=None, doPyrDown=False, witcloudComponentName='VideoReader'):
        WitcloudComponent.__init__(self, witcloudComponentName=witcloudComponentName)
        threading.Thread.__init__(self)
        self.cap = skvideo.io.vreader(filename)
        self.ret = False
        self.frame = None
        self.doPyrDown = doPyrDown
        self.frame_id = 1
        self.first_frame = self.cap.__next__()
        self.full_height = self.first_frame.shape[0]
        self.full_width = self.first_frame.shape[1]
        self.init_datetime = video_date if video_date else datetime.utcnow()
        self.flag = True
        self.id_frame = 0
        self.id_video = 0
        self.frame_rate = frame_rate
        assert frame_rate > 0

    def new_frame(self):
        try:            
            data = self.cap.__next__()
            if self.doPyrDown:
                data = resize(data, (data.shape[0] / 4, data.shape[1] / 4))
            self.init_datetime += timedelta(microseconds=self.frame_id*self.frame_rate / 1E6)
            self.frame = Frame(data=data, timestamp=time.mktime(self.init_datetime.timetuple()) + self.init_datetime.microsecond / 1E6, id=self.frame_id)
            self.frame_id+=1
            return self.frame
        except:
            return None

    def run(self):
        print('Started')
        while self.flag:
            newFrame = self.new_frame()
            if not newFrame:
                return
            self.id_frame += 1
            newFrame.id = self.id_frame
            newFrame.id_video=self.id_video
            self.send(newFrame)
            init_time = time.time()
            time.sleep(0.2)
        print('video end')

    def stop(self):
        print('Stopping Thread')
        self.flag = False
