from witcloud.modules.WitcloudComponent import WitcloudComponent

from witcloud.models.Frame import Frame
import numpy as np
from witcloud.util import util,image

class ImageWriter(WitcloudComponent):
	def __init__(self, filename=None, inputdict=None, outputdict=None):
		WitcloudComponent.__init__(self)

	def update(self, frame:Frame):
		imagename = util.filename_generator(TMP_FOLDER="/tmp").__next__()
		image.save_image(imagename, frame.data)
		return imagename
