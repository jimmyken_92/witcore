import sys
import os
import time
import numpy as np
cpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(cpath, '../../'))

from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
from witcloud.models.Roi import Roi
# global variables from __init__.py file
from witcloud.modules import LOGGER_LEVEL
#--------- LOGGING PARAMS -----------------#
import logging
from witcloud.modules import LOGGER_LEVEL
LOGGER_FORMAT = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMAT)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
# ---------- LOGGING PARAMS ---------------- #

class FrameCropper(WitcloudComponent):
    """ Params
        ------
            receives a frame.
    """

    def __init__(self, roi_list):
        WitcloudComponent.__init__(self, witcloudComponentName="FrameCropper_component")
        self.roi_list = roi_list
        self.frame = None
        self.cropped_frames = {}

    def update(self, frame):
        assert type(frame) is Frame
        #check if there is no roi in the roilist
        if type(self.roi_list) is list and type(self.roi_list[0]) is dict:
            if 'xywh' in self.roi_list[0].keys() and len(self.roi_list[0]['xywh'])==0:
                height, width = frame.data.shape[:2]
                self.roi_list[0]['xywh'] = [(0,0,width,height)]
                self.roi_list[0]['id'] = 1

        all(isinstance(x, Roi) for x in self.roi_list)

        self.frame = Frame(data=frame.data.copy(), timestamp=frame.timestamp)
        for roi in self.roi_list:
            if type(roi) is dict:
                x, y, w, h = roi['xywh'][0]
                id = roi['id']
            else:
                x, y, w, h = roi.xywh
                id = roi.id
            self.cropped_frames[id] = {}
            cropped = frame.data.copy()
            newFrameCropped = Frame(data=cropped[y:y+h, x:x+w, :], timestamp=self.frame.timestamp)
            self.cropped_frames[id]['cropped_frame'] = newFrameCropped

        self.send(self.cropped_frames)