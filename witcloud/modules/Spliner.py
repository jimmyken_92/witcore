from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
import numpy as np
from witcloud.util import util,image
from witcloud.models.Detection import Detection
from witcloud.models.InertialDetection import InertialDetection
from witcloud.models.Spline import Spline

import numbers
import numpy as np
from numpy.linalg import inv
from scipy import misc
import scipy.interpolate
from scipy.interpolate import UnivariateSpline
from scipy.optimize import linear_sum_assignment
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path
import colorsys
import time
import random



"""
MODELS: 

#DETECTION#  List of 4 numbers [x,y,w,h] representing a detection
    x: first coordinate
    y: second coordinate
    w: width 
    h: height

#INERTIAL_DETECTION#  List of 7 numbers [x,y,w,h,mx,my,t] representing a detection with an inferred momentum
    x: first coordinate
    y: second coordinate
    w: width 
    h: height
    mx: inferred x-momentum
    my: inferred y-momentum
    t: time(stamp)

#SNAPSHOT#  List of (supposed time-sharing) #DETECTION#s

#SPLINE#  List of (time-ordered) #INERTIAL_DETECTION#s

#CURVE#  List of type [(interp_x, interp_y, interp_w, interp_h), start_t, end_t] (interpolated #SPLINE#)
    interp_x: interpolating function of x-coordinate (scipy.interpolate.UnivariateSpline)
    interp_y: interpolating function of y-coordinate (scipy.interpolate.UnivariateSpline)
    interp_w: interpolating function of w-coordinate (scipy.interpolate.UnivariateSpline)
    interp_h: interpolating function of h-coordinate (scipy.interpolate.UnivariateSpline)
    start-t: initial time(stamp)
    end-t: final time(stamp)

"""

### UNIT TESTS ###

def isDetection(x):
    assert type(x) == list, '{} is not of type list'.format(x)
    assert len(x) == 4, '{} is not of length 4'.format(x)
    assert isinstance(x[0], numbers.Number), 'First element of {} is not numeric'.format(x)
    assert isinstance(x[1], numbers.Number), 'Second element of {} is not numeric'.format(x)
    assert isinstance(x[2], numbers.Number), 'Third element of {} is not numeric'.format(x)
    assert isinstance(x[3], numbers.Number), 'Fourth element of {} is not numeric'.format(x)

def isInertialDetection(x):
    assert type(x) == list, '{} is not of type list'.format(x)
    assert len(x) == 7, '{} is not of length 7'.format(x)
    assert isinstance(x[0], numbers.Number), 'First element of {} is not numeric'.format(x)
    assert isinstance(x[1], numbers.Number), 'Second element of {} is not numeric'.format(x)
    assert isinstance(x[2], numbers.Number), 'Third element of {} is not numeric'.format(x)
    assert isinstance(x[3], numbers.Number), 'Fourth element of {} is not numeric'.format(x)
    assert isinstance(x[4], numbers.Number), 'Fifth element of {} is not numeric'.format(x)
    assert isinstance(x[5], numbers.Number), 'Sixth element of {} is not numeric'.format(x)
    assert isinstance(x[6], numbers.Number), 'Seventh element of {} is not numeric'.format(x)

def isSnapshot(x):
    assert type(x) == list, '{} is not of type list'.format(x)
    for k in x:
        isDetection(k)

def isSpline(x):
    assert type(x) == list, '{} is not of type list'.format(x)
    for k in x:
        isInertialDetection(k)

def isCurve(x):
    assert type(x) == list, '{} is not of type list'.format(x)
    assert len(x) == 3, '{} is not of length 3'.format(x)
    assert isinstance(x[1], numbers.Number), 'Second element of {} is not numeric'.format(x)
    assert isinstance(x[2], numbers.Number), 'Third element of {} is not numeric'.format(x)
    assert type(x[0]) == list, 'First element of {} is not a list'.format(x)
    assert len(x[0]) == 4, 'First element of {} is not of length 4'.format(x)
    # assert isinstance(x[0][0],scipy.interpolate.fitpack2.LSQUnivariateSpline), 'First element of interpolations of {} is not a UnivariateSpline'.format(x)
    # assert isinstance(x[0][1],scipy.interpolate.fitpack2.LSQUnivariateSpline), 'Second element of interpolations of {} is not a UnivariateSpline'.format(x)
    # assert isinstance(x[0][2],scipy.interpolate.fitpack2.LSQUnivariateSpline), 'Third element of interpolations of {} is not a UnivariateSpline'.format(x)
    # assert isinstance(x[0][3],scipy.interpolate.fitpack2.LSQUnivariateSpline), 'Fourth element of interpolations of {} is not a UnivariateSpline'.format(x)

### ~~~~~~~~~~ ###

def dist(p:InertialDetection, q:Detection, pixels_per_frame, time):
    """
    Compute distance between two space-time points

    :param p: First point of type #INERTIAL_DETECTION#
    :param q: Second point of type #DETECTION#
    :param pixels_per_frame: Number of pixels of expected translation per time(stamp) unit
        for a medium-velocity event
    :param time: Number of frame - timestamp of second detection (q)

    :return: computed distance
    """

    #~~#
    # isInertialDetection(p)
    # isDetection(q)
    assert isinstance(p, InertialDetection)
    assert isinstance(q, Detection)
    assert isinstance(pixels_per_frame, numbers.Number), 'pixels_per_frame is not numeric'
    assert isinstance(time, numbers.Number), 'time is not numeric'
    #~~#

    # return (p.x+p.height*(time-p.timestamp)-q.center_x)**2 +\
    #        (p.y+p.my*(time-p.timestamp)-q.center_y)**2 +\
    #        (np.log(p.y)-np.log(q.center_y))**2*3000 +\
    #        (np.log(p.width)-np.log(q.width))**2*3000 +\
    #        pixels_per_frame**2*(p.timestamp-time)**2 +\
    #        int(time == p.timestamp)*10**6

    return (p.x+p.height*(time-p.timestamp)-q.center_x) +\
           (p.y+p.my*(time-p.timestamp)-q.center_y) +\
           pixels_per_frame*(p.timestamp-time) +\
           int(time == p.timestamp)*10**6

def spliner(snapshot, cache, ts, dist_thres, pixels_per_frame):
    """
    Assign the detections in a frame with splines of previous frames

    :param snapshot: #SNAPSHOT# of current frame
    :param cache: List of (open) #SPLINE#s
    :param ts: time(stamp)
    :param dist_thres: Distance threshold
    :param pixels_per_frame: Number of pixels of expected translation per time(stamp) unit 
        for a medium-velocity event

    :return: A tuple (closed_splines, new_cache)
        closed_splines: List of (closed) #SPLINE#s
        new_cache: List of (open) #SPLINE#s
    """

    #~~#
    # isSnapshot(snapshot)
    # assert type(cache) == list, 'cache is not a list'
    # for k in cache:
    #     isSpline(k)
    # assert isinstance(time, numbers.Number), 'time is not numeric'
    # assert isinstance(dist_thres, numbers.Number), 'dist_thres is not numeric'
    # assert isinstance(pixels_per_frame, numbers.Number), 'pixels_per_frame is not numeric'
    #~~#

    # if there are no open splines, everything goes into cache
    if len(cache)==0:
        for i, detection in enumerate(snapshot):
            spline = Spline(id=random.randint(0, 100))
            spline.add_node(InertialDetection(
                    x=detection.center_x,
                    y=detection.center_y,
                    width=detection.width,
                    height=detection.height,
                    timestamp=detection.timestamp,
                    mx=0, my=0))
            # cache.append({'id':random.randint(0, 100), 'coordinates':[[x,y,w,h,0,0,ts]]})
            cache.append(spline)
    else:
        distance_matrix = np.empty([len(snapshot),len(cache)])
        # we loop over all the detections in the frame
        for i, detection in enumerate(snapshot): #range(len(snapshot))
            # [x,y,w,h] = snapshot[i]
            # x = detection.center_x
            # y = detection.center_y
            # w = detection.width
            # h = detection.height
            # and over all open splines in cache
            for j in range(len(cache)):
                distance_matrix[i,j] = dist(cache[j].coordinates[-1],detection,pixels_per_frame,ts)

        # compute the best pairing
        row_ind, col_ind = linear_sum_assignment(distance_matrix)

        # and implement the assignments
        for (row,col) in zip(row_ind, col_ind):
            if distance_matrix[row,col] < dist_thres:
                # [x,y,w,h] = snapshot[row]
                i_d = InertialDetection(
                        x=snapshot[row].center_x,
                        y=snapshot[row].center_y,
                        width=snapshot[row].width,
                        height=snapshot[row].height,
                        timestamp=snapshot[row].timestamp,
                        mx=0.9*cache[col].coordinates[-1].mx+0.1*(snapshot[row].center_x-cache[col].coordinates[-1].x)/(ts-cache[col].coordinates[-1].timestamp+1),
                        my=0.9*cache[col].coordinates[-1].my+0.1*(snapshot[row].center_y-cache[col].coordinates[-1].y)/(ts-cache[col].coordinates[-1].timestamp+1)
                )
                cache[col].coordinates.append(i_d)
            else:
                # add new spline
                # [x,y,w,h] = snapshot[row]
                # cache.append({'id':random.randint(0, 100), 'coordinates':[[x,y,w,h,0,0,ts]]})
                spline = Spline(id=random.randint(0, 100))
                spline.add_node(InertialDetection(
                    x=snapshot[row].center_x,
                    y=snapshot[row].center_y,
                    width=snapshot[row].width,
                    height=snapshot[row].height,
                    timestamp=snapshot[row].timestamp,
                    mx=0, my=0))
                cache.append(spline)

        # all other detections start a new spline
        for i in [idx for idx in range(len(snapshot)) if idx not in row_ind]:
            # [x,y,w,h] = snapshot[i]
            # cache.append({'id':random.randint(0, 100), 'coordinates':[[x,y,w,h,0,0,ts]]})
            spline = Spline(id=random.randint(0, 100))
            spline.add_node(InertialDetection(
                    x=snapshot[row].center_x,
                    y=snapshot[row].center_y,
                    width=snapshot[row].width,
                    height=snapshot[row].height,
                    timestamp=snapshot[row].timestamp,
                    mx=0, my=0))
            cache.append(spline)
                
    closed_splines = []
    # each spline in cache is checked
    for k in range(len(cache)-1,-1,-1): ### reverse lookup to avoid conflicts with pop
        # and closed if the edge is too far in ts to the actual ts
        # if pixels_per_frame*(ts - cache[k].coordinates[-1].timestamp)**2 > dist_thres:
        if ts - cache[k].coordinates[-1].timestamp > 10*60:
            closed_splines.append(cache.pop(k)) ### from cache to closed_splines

    return (closed_splines, cache)

class Spliner(WitcloudComponent):
    def __init__(self, dist_thres, pixels_per_frame):
        WitcloudComponent.__init__(self)
        self.dist_thres = dist_thres
        self.pixels_per_frame = pixels_per_frame
        self.cache = []
        self.buffer = [] # used to temp storage of Detections

    def update(self, detection: Detection):
        if len(self.buffer):
            if self.buffer[-1].timestamp != detection.timestamp:
                self.update_list_detections(self.buffer)
                self.buffer = []
        self.buffer.append(detection)

    def update_list_detections(self, list_detections: 'List of Detections'):
        # print("pushing list-deteitons", list_detections)
        closed_splines, self.cache = spliner(snapshot=list_detections, cache=self.cache, ts=list_detections[0].timestamp, 
                dist_thres=self.dist_thres, pixels_per_frame=self.pixels_per_frame)
        if len(closed_splines) > 0:
            for spline in closed_splines:
                self.send(spline)

    def close(self):
        for spline in self.cache:
            self.send(spline)
