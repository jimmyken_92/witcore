from __future__ import division, print_function, absolute_import

from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
import cv2
import threading
import os

class DetectorDrawer(WitcloudComponent, threading.Thread):
    def __init__(self, output):
        WitcloudComponent.__init__(self, witcloudComponentName="DetectorDrawer")
        threading.Thread.__init__(self)
        print("### DetectorDrawer Started ###")

        self.output = output

    def run(self):
        if not os.path.exists(self.output):
            os.makedirs(self.output)

    def writeFrame(self, frame):
        if not os.path.exists("/tmp/drawer"): os.makedirs("/tmp/drawer")
        filename = "%02d.png" % (frame.id)
        cv2.imwrite("/tmp/drawer/" + filename, frame.data)

    def drawer(self, detections, frame):
        # self.writeFrame(frame)

        try:
            for detection in detections:
                overlay_text = "%s" % (detection.label)
                cv2.rectangle(frame.data, (int(detection.x1), int(detection.y1)), (int(detection.x2), int(detection.y2)), (0, 255, 0), 2)
                cv2.putText(frame.data, overlay_text, (int(detection.x1), int(detection.y1)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

            # self.writeFrame(frame)
            print("INFO:DetectorDrawer: Draw a new detection")
            self.send(frame)
        except Exception as e:
            print("ERROR:DetectorDrawer: Error while saving detection file by DetectionDrawer --> %s" % str(e))

    def update(self, data):
        print("INFO:DetectorDrawer: Received a new detection")
        detections, frame = data
        self.drawer(detections, frame)
