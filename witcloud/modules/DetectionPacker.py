from witcloud.models.Detection import Detection
from witcloud.modules.WitcloudComponent import WitcloudComponent

import json
import os
import time

class DetectionPacker(WitcloudComponent):
    def __init__(self, filename):
        WitcloudComponent.__init__(self, witcloudComponentName="DetectionPacker")

        self.filename = filename
        self.list_detections = []

    def update(self, data):
        detection, frame = data
        self.list_detections.append(detection)
        print("INFO:DetectionPacker: Received a new detection")

    def dumpDetectionsToFile(self):
        jsonlist = []
        print("### DetectionPacker Started ###")

        if self.list_detections is not None:
            detections = self.list_detections
            try:
                if not os.path.exists(os.path.dirname(self.filename)):
                    os.mkdir(os.path.dirname(self.filename))

                with open(self.filename, "w+") as outfile:
                    for detection in detections:
                        for obj in detection:
                            jsonlist.append(obj.__dict__)

                    json.dump(jsonlist, outfile, indent=4)

                print("Saved detections in file --> %s" % self.filename)
            except Exception as e:
                print("ERROR:Error while saving detection file by detection packer --> %s" % str(e))
        else:
            print("Nothing found in detections list")
