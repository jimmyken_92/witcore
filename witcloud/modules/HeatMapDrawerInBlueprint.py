from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
from witcloud.models.HeatMap import HeatMap
import witcloud.util as util
import matplotlib.pyplot as plt
import math
import numpy as np
import cv2
import time
import threading

#--------- LOGGING PARAMS -----------------#
from witcloud.modules import LOGGER_LEVEL
from witcloud.modules import LOGGER_FORMATTER
import logging
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler(__name__ + ".log")
hdlr.setFormatter(LOGGER_FORMATTER)
logger.addHandler(hdlr)
logger.setLevel(LOGGER_LEVEL)
# ---------- LOGGING PARAMS ---------------- #

# ---------- TEST PARAMS ---------------- #
from heatmappy import Heatmapper
from witcloud.modules.ImageWriter import ImageWriter
from PIL import Image
# ---------- TEST PARAMS ---------------- #


class HeatMapDrawerInBlueprint(WitcloudComponent, threading.Thread):
    def __init__(self, real_width, real_height, num_margin=2, pixel_per_meter=50, radius=20, variance=150):
        WitcloudComponent.__init__(self)
        threading.Thread.__init__(self)
        self.pixel_per_meter = pixel_per_meter
        self.margin = num_margin*self.pixel_per_meter
        self.real_width = real_width
        self.real_height = real_height
        self.filename = '/tmp/heatmap_%s.png'
        self.variance = variance
        self.radius = radius
        self.last_timestamp = None
        self.flag = True  # flag to control the main thread
        self.heatmap = None

    def run(self):
        logger.info("Started")
        while(self.flag):
            if self.heatmap:
                hm = self.draw(self.heatmap)
                self.send(hm)
                hm = np.array(hm)
                cv2.imshow('visitors heatmap', hm)
                cv2.waitKey(100)
            time.sleep(0.5)
        # end
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def update(self, heatmap: HeatMap):
        logger.debug("Drawing heatmap")
        self.heatmap = heatmap

    def draw(self, heatmap):
        # heatmap draw
        logger.info("Heatmap drawn")
        img = ImageWriter().update(heatmap.frame)
        ###
        imgpath = Image.open(img)
        if imgpath:
            coordinates = []
            for data in heatmap.frame.data:
                for coordinate in data:
                    coordinates.append((coordinate[0], coordinate[1]))
            heatmapper = Heatmapper()
            heatmap = heatmapper.heatmap_on_img(coordinates, imgpath)
            heatmap.save("/tmp/heatmap.png") # self.filename % str(self.last_timestamp) + 
            # print(len(imgpath))
            return heatmap
