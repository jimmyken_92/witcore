from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
import numpy as np
from witcloud.util import util, image
from witcloud.models.Spline import Spline

import numbers
import numpy as np
from numpy.linalg import inv
import time
import random
import copy

def get_projective_transformation(start_points, end_points):
    """
    Get projective transformation matrix transforming four initial points into four final points

    :param start_points: numpy array of shape (4,2)
    :param end_points: numpy array of shape (4,2)

    :return: A numpy array (matrix) of shape (3,3)
    """

    #~~#
    assert isinstance(start_points,np.ndarray), 'start_points is not a numpy array'
    assert start_points.shape == (4,2), 'start_points is not of shape (4,2)'
    assert isinstance(end_points,np.ndarray), 'end_points is not a numpy array'
    assert end_points.shape == (4,2), 'end_points is not of shape (4,2)'
    #~~#

    # documented from 
    # https://math.stackexchange.com/questions/296794/finding-the-transform-matrix-from-4-projected-points-with-javascript

    # extracting coordinates
    sx0 = start_points[0,0]
    sy0 = start_points[0,1]
    sx1 = start_points[1,0]
    sy1 = start_points[1,1]
    sx2 = start_points[2,0]
    sy2 = start_points[2,1]
    sx3 = start_points[3,0]
    sy3 = start_points[3,1]
    ex0 = end_points[0,0]
    ey0 = end_points[0,1]
    ex1 = end_points[1,0]
    ey1 = end_points[1,1]
    ex2 = end_points[2,0]
    ey2 = end_points[2,1]
    ex3 = end_points[3,0]
    ey3 = end_points[3,1]

    v1 = np.dot(inv(np.matrix([[1,1,1],[sx0,sx1,sx2],[sy0,sy1,sy2]])), np.matrix([[1],[sx3],[sy3]]))
    diagv1 = np.diag([v1[0,0],v1[1,0],v1[2,0]])
    A = np.dot(np.matrix([[1,1,1],[sx0,sx1,sx2],[sy0,sy1,sy2]]), diagv1)

    v2 = np.dot(inv(np.matrix([[1,1,1],[ex0,ex1,ex2],[ey0,ey1,ey2]])), np.matrix([[1],[ex3],[ey3]]))
    diagv2 = np.diag([v2[0,0],v2[1,0],v2[2,0]])
    B = np.dot(np.matrix([[1,1,1],[ex0,ex1,ex2],[ey0,ey1,ey2]]), diagv2)

    return np.dot(B,inv(A))

def direct_transform(matrix, point):
    """
    Transform a point according to a projective transformation

    :param matrix: A numpy array (matrix) of shape (3,3) representing the transformation
    :param point: list [x,y] of coordinates of the point to be mapped

    :return: list [x',y'] of coordinates of the mapped point
    """

    #~~#
    assert isinstance(matrix,np.ndarray), 'matrix is not a numpy array'
    assert matrix.shape == (3,3), 'matrix is not of shape (3,3)'
    assert type(point) == list, 'point is not a list'
    assert len(point) == 2, 'point is not of length 2'
    assert isinstance(point[0], numbers.Number), 'First element of point is not numeric'
    assert isinstance(point[1], numbers.Number), 'Second element of point is not numeric'
    #~~#

    v = [1,point[0],point[1]]
    w = np.dot(matrix,v)
    return [w[0,1]/w[0,0],w[0,2]/w[0,0]]

class PointTransformer(WitcloudComponent):
    def __init__(self, start_points, end_points):
        WitcloudComponent.__init__(self)
        self.start_points = start_points
        self.end_points = end_points
        # COMPUTE PROJECTIVE TRANSFORMATION MATRIX #
        self.proj_trans_matrix = get_projective_transformation(self.start_points, self.end_points)

    def update(self, spline:Spline):
        # TRANSFORM SPLINE #
        transformedSpline = Spline(id=spline.id)
        for inertialDetection in spline.coordinates:
            print("before transformation --> ", )
            print((inertialDetection.y, inertialDetection.x))
            (inertialDetection.y, inertialDetection.x) = direct_transform(matrix=self.proj_trans_matrix,
                                                point=[inertialDetection.y+inertialDetection.height/2, inertialDetection.x])
            print("after transformation --> ", )
            print((inertialDetection.y, inertialDetection.x))
            transformedSpline.coordinates.append(inertialDetection)
        self.send(transformedSpline)
