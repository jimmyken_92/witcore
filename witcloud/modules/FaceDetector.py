from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Detection import Detection
from witcloud.models.Frame import Frame

import face_recognition

class FaceDetector(WitcloudComponent):
	def __init__(self, LABEL=None):
		WitcloudComponent.__init__(self)
		print("### FaceDetector Started ###")

		self.LABEL = LABEL

	def recogniseFace(self, newFrame):
		if newFrame is not None:
			print("INFO:FaceDetector: Recognise face")
			frame = newFrame.data
			faces = []

			if self.LABEL is not None:
				# recognise face
				detected_faces = face_recognition.face_locations(frame)
				# encode data
				encodings = face_recognition.face_encodings(frame, detected_faces)

				# reorder list
				for top, right, bottom, left in detected_faces:
					x1, x2, y1, y2 = left, right, top, bottom
					faces.append([x1, x2, y1, y2])

				self.send((faces, encodings, newFrame))
			else:
				# recognise face
				detected_faces = face_recognition.face_locations(frame)

				# reorder list
				for top, right, bottom, left in detected_faces:
					x1, x2, y1, y2 = left, right, top, bottom
					faces.append([x1, x2, y1, y2])

				# return the detected detected faces
				self.send((faces, newFrame))

	def update(self, frame:Frame):
		self.recogniseFace(frame)
