# global variables from __init__.py file
from witcloud.modules.WitcloudComponent import WitcloudComponent

from witcloud.models.Frame import Frame
import os, sys
import threading
import numpy as np
import time
import platform
import subprocess as sp
import requests
import json
# import skvideo.io
from witcloud.media.xmltodict import parse as xmltodictparser
from witcloud.modules.FfmpegReader import *

class CameraReader(WitcloudComponent, threading.Thread):
    def __init__(self, filename, cam_id, url_probe=None,
                    inputdict=None, outputdict=None,
                    frames_to_sample=10, frame_rate=1.0,
                    witcloudComponentName="CameraReader"):
        WitcloudComponent.__init__(self, witcloudComponentName)
        threading.Thread.__init__(self) #, None, None)
        self.filename = str(filename)
        if url_probe:
            self.url_probe = str(url_probe)
        else: 
            self.url_probe = str(filename)
        self.cam_id = cam_id
        print("Video source: [%s]"%self.filename)
        print("Video source for ffprobe: [%s]"%self.url_probe)
        try:
            self.reader = FFmpegReader(filename=self.filename, url_probe=self.url_probe, inputdict=inputdict, outputdict=outputdict, cam_id=self.cam_id)
        except Exception as e:
            self.errors.append("Error while creating FFmpegReader [%s]"%str(e))
            raise Exception("Error while creating FFmpegReader")
        self.frames_to_sample = frames_to_sample
        self.frame_rate = frame_rate
        self.flag = True
        self.buffer = []
        self.id_video = 0  #no video yet
        self.id_frame = 0  #no frame yet

    def stop(self):
        """ Stop the thread """
        print("Stopping Thread")
        self.flag = False

    def run(self):
        """ Run the thread """
        print("Started")
        init_time = time.time()
        self.update_new_video()
        while self.flag:
            print("video frame read")
            newFrame = self.reader._readFrame()
            self.id_frame += 1
            newFrame.id = self.id_frame
            newFrame.id_video=self.id_video
            if time.time() - init_time > float(1.0/self.frame_rate):
                if self.id_frame % 100 ==0:
                    print("accept new frame time-spaced [%s] seconds"%str(self.frame_rate))
                self.send(newFrame)
                init_time = time.time()
                # if len(self.buffer) > 1:
                #     self.normalize_tstamp()
            time.sleep(0.005)
            
        print("stopped grabbing")

    # def normalize_tstamp(self):
    #     if len(self.buffer)>1:
    #         # time_elapsed = self.buffer[-1].timestamp - self.buffer[0].timestamp
    #         # time_delta = time_elapsed / (len(self.buffer)-1)
    #         # for i, _ in enumerate(self.buffer):
    #         #     self.buffer[i].timestamp = self.buffer[0].timestamp + i*float(time_delta)
    #         # print("camera reader notifying frames with no normalisation of timestamp!!!")
    #         for frame in self.buffer:
    #             # Notify
    #             self.send(frame)
    #         #new video
    #         self.buffer.clear()
    #         # self.id_video = self._get_id_video()
    #     else:
    #         print("NO FRAMES IN BUFFER OF CAMERA READER")
    #         logger.error("NO FRAMES IN BUFFER OF CAMERA READER...... WAIT PLEASE.... :8)")

    def update_new_video(self):
        self.id_video = self._get_id_video()
        self.id_frame = 0
        
    def _estimate_fps(self):
        print("estimating fps .... please wait..")
        frame_generator = reader.frame_generator()
        ini = time.time()
        for i in range(self.frames_to_sample):
            print("inside estimate fps...... %d"%i)
            frame_generator.next()
        elapsed = time.time() - ini
        fps = self.frames_to_sample/elapsed
        print("fps computation : ", fps)
        return fps

    def grabAFrame(self):
        newFrame = self.reader._readFrame()
        # Notify
        self.send(newFrame)

    def _get_id_video(self):
        video_id = "%s_%s"%(str(self.cam_id), str(int(time.time()*1000)))
        print("New video id generated --> %s"%video_id)
        return video_id
