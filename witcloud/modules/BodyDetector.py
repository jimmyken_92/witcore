from __future__ import division, print_function, absolute_import

from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
import numpy as np

# ---------- DETECTOR PARAMS ---------------- #
import warnings
from PIL import Image
from witcloud.util.yolo import YOLO

from witcloud.util.deep_sort import preprocessing
from witcloud.util.deep_sort.detection import Detection as dsd
from witcloud.util.tools import generate_detections as gdet
from witcloud.util import models
# ---------- DETECTOR PARAMS ---------------- #

class BodyDetector(WitcloudComponent):
    def __init__(self):
        WitcloudComponent.__init__(self)

        self.yolo = YOLO()
        self.nms_max_overlap = 1.0

        modelFile = models.pbModel()
        self.encoder = gdet.create_box_encoder(modelFile, batch_size=1)

    def detection(self, newFrame):
        warnings.filterwarnings("ignore")
        if newFrame is not None:
            print("INFO:BodyDetector: Reading frame...")
            frame = newFrame.data

            image = Image.fromarray(frame)
            boxs = self.yolo.detect_image(image)
            features = self.encoder(frame, boxs)

            detections = [dsd(bbox, 1.0, feature) for bbox, feature in zip(boxs, features)]

            boxes = np.array([d.tlwh for d in detections])
            scores = np.array([d.confidence for d in detections])
            indices = preprocessing.non_max_suppression(boxes, self.nms_max_overlap, scores)
            detections = [detections[i] for i in indices]

            self.send((detections, newFrame))

    def update(self, frame:Frame):
        self.detection(frame)
