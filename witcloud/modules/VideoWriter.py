from witcloud.modules.WitcloudComponent import WitcloudComponent
from witcloud.models.Frame import Frame
from witcloud.util.FFmpegWriter import FFmpegWriter
from witcloud.util import util

import sys

class VideoWriter(WitcloudComponent):
	def __init__(self, output):
		WitcloudComponent.__init__(self)
		print("### VideoWriter Started ###")

		self.Flag = True
		self.filename = output
		self.frames = []

		self.videoWriter = FFmpegWriter(filename=self.filename)

	def update(self, frame):
		try:
			fr = frame.data.copy()
			self.videoWriter.writeFrame(fr)
		except Exception as e:
			sys.stderr.write()

	def close(self):
		self.videoWriter.close()
